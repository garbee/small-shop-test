<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping', function (Blueprint $table) {
            $table->uuid('id')->unique();

            $table->decimal('width', 8 , 2);
            $table->decimal('height', 8 , 2);
            $table->decimal('length', 8 , 2);
            $table->decimal('weight', 8 , 2);

            $table->uuid('product_id')->nullable();
            $table->uuid('product_variation_id')->nullable();

            $table->timestamps();

            $table->primary('id');

            $table->unique('product_id');
            $table->unique('product_variation_id');

            $table->foreign('product_id')
                  ->references('id')
                  ->on('products')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->foreign('product_variation_id')
                  ->references('id')
                  ->on('product_variations')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping');
    }
}
