<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductOptionSetOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_option_set_options', function (Blueprint $table) {
            $table->uuid('option_set_id');
            $table->uuid('option_id');

            $table->primary(['option_set_id', 'option_id']);

            $table->foreign('option_set_id')
                ->references('id')
                ->on('product_option_sets')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('option_id')
                ->references('id')
                ->on('product_options')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_option_set_options');
    }
}
