<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->uuid('id');

            $table->string('name');

            $table->uuid('parent_id')->nullable();
            $table->unsignedInteger('left');
            $table->unsignedInteger('right');

            $table->timestamps();

            $table->primary('id');
            $table->unique(['name', 'parent_id']);
            $table->index([
                'parent_id',
                'left',
                'right',
            ]);
            $table->foreign('parent_id')
                ->references('id')
                ->on('states')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
