<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->uuid('id');

            $table->string('token')->unique();
            $table->boolean('charged');
            $table->boolean('attempted');
            $table->boolean('authorized');
            $table->integer('amount');
            $table->string('charge_id')->unique();
            $table->string('email')->nullable();
            $table->uuid('order_id');

            $table->timestamps();

            $table->primary('id');

            $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
