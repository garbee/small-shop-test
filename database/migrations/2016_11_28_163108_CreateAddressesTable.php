<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->uuid('id');

            $table->string('street_one');
            $table->string('street_two')->nullable();
            $table->string('city');
            $table->string('postcode');
            $table->string('company')->nullable();
            $table->string('phone')->nullable()
                ->comment('Phone number for contact at address for postal service to help ensure delivery.');

            $table->uuid('state_id');

            $table->timestamps();

            $table->primary('id');

            $table->foreign('state_id')
                ->references('id')
                ->on('states')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
