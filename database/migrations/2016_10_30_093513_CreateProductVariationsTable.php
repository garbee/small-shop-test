<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductVariationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_variations', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->uuid('product_id');

            $table->integer('price')->nullable();
            $table->integer('units_in_stock')->default(0);
            $table->boolean('disabled')->default(false);
            $table->boolean('default')->default(false);

            $table->timestamps();
            $table->softDeletes();

            $table->primary('id');
            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variations');
    }
}
