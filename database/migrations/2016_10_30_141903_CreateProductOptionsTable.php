<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_options', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('group_id');

            $table->string('value');
            $table->string('name');

            $table->timestamps();
            $table->primary('id');

            $table->unique([
                'group_id',
                'value',
            ]);
            $table->foreign('group_id')
                ->references('id')
                ->on('product_option_groups')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_options');
    }
}
