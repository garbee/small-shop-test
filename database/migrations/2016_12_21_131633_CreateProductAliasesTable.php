<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductAliasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_aliases', function (Blueprint $table) {
            $table->uuid('product_id');
            $table->uuid('alias_id');

            $table->primary(['product_id', 'alias_id']);

            $table->foreign('alias_id')
                  ->references('id')
                  ->on('aliases')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->foreign('product_id')
                  ->references('id')
                  ->on('products')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_aliases');
    }
}
