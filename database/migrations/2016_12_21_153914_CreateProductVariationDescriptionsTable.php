<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductVariationDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_variation_descriptions', function (Blueprint $table) {
            $table->uuid('variation_id');
            $table->uuid('description_id');

            $table->primary(['variation_id', 'description_id']);

            $table->foreign('variation_id')
                  ->references('id')
                  ->on('product_variations')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->foreign('description_id')
                  ->references('id')
                  ->on('descriptions')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variation_descriptions');
    }
}
