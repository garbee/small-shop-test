<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVariationOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_variation_options', function (Blueprint $table) {
            $table->uuid('product_variation_id');
            $table->uuid('product_option_id');

            $table->primary([
                'product_variation_id',
                'product_option_id',
            ]);

            $table->foreign('product_variation_id')
                ->references('id')
                ->on('product_variations')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('product_option_id')
                ->references('id')
                ->on('product_options')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variation_options');
    }
}
