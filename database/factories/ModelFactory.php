<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\State;
use Faker\Generator;
use Money\Currency;
use PhpUnitsOfMeasure\PhysicalQuantity\Length;
use PhpUnitsOfMeasure\PhysicalQuantity\Mass;

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name'           => $faker->name,
        'email'          => $faker->safeEmail,
        'password'       => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Product::class, function (Faker\Generator $faker) {
    return [
        'name'  => $faker->words(random_int(1, 4), true),
        'price' => new Money\Money(
            random_int(0, 10000),
            new Money\Currency(\App\DEFAULT_CURRENCY)
        ),
        'units_in_stock' => random_int(1, 100),
        'disabled' => (bool) random_int(0, 1),
    ];
});

$factory->define(App\Product\Variation::class, function (Faker\Generator $faker) {
    return [
        'units_in_stock' => random_int(0, 100),
        'disabled' => (bool) random_int(0, 1),
        'price' => new Money\Money(random_int(0, 10000), new Currency(\App\DEFAULT_CURRENCY)),
    ];
});

$factory->define(App\Shipping::class, function (Faker\Generator $faker) {
    return [
        'weight' => new Mass($faker->randomFloat(0.5, 0, 8), 'ounces'),
        'width' => new Length($faker->randomFloat(2, 0, 12), 'inches'),
        'height' => new Length($faker->randomFloat(2, 0, 12), 'inches'),
        'length' => new Length($faker->randomFloat(2, 0, 12), 'inches'),
    ];
});

$factory->define(App\Description::class, function (Faker\Generator $faker) {
    return [
        'content' => $faker->paragraphs(random_int(1, 4), true),
    ];
});

$factory->define(App\Seo::class, function (Faker\Generator $faker) {
    $data = [];

    if ((bool) random_int(0, 1)) {
        $data['title'] = $faker->sentence(random_int(3, 9));
    }

    if ((bool) random_int(0, 1)) {
        $data['description'] = $faker->sentence(random_int(9, 23));
    }

    return $data;
});

$factory->define(\App\Alias::class, function (Faker\Generator $faker) {
    return [
        'content' => $faker->unique()->numerify('#############')
    ];
});

$factory->define(\App\Address::class, function(Generator $faker) {
    /** @var State $state */
    $state = State::all()->random(1)->first();
    return [
        'street_one' => $faker->streetAddress,
        'city' => $faker->city,
        'postcode' => $faker->postcode,
        'company' => random_int(0, 1) ? $faker->company : null,
        'phone' => random_int(0, 1) ? $faker->phoneNumber : null,
        'state_id' => $state->id,
    ];
});
