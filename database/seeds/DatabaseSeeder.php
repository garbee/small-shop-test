<?php

use App\Address;
use App\Alias;
use App\Order;
use App\Order\Status;
use App\Seo;
use App\Product;
use App\Shipping;
use App\Description;
use App\State;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Money\Currency;
use Money\Money;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedStates();
        $this->seedOptions();
        $this->seedOptionSets();
        $this->seedOrderStatuses();

        factory(User::class, 30)->create()->each(function (User $user) {
            if ((bool) random_int(0, 1)) {
                $user->addresses()->saveMany(factory(Address::class, random_int(2, 6))->make());
            }
        });

        /** @var Collection $products */
        $products = factory(Product::class, 100)->create()->each(function(Product $product) {
            $product->shipping()->save(factory(Shipping::class)->make());
            if ((bool) random_int(0, 1)) {
                $product->descriptions()->save(factory(Description::class)->make(['type' => 'short']));
            }
            if ((bool) random_int(0, 1)) {
                $product->descriptions()->save(factory(Description::class)->make(['type' => 'long']));
            }
            if ((bool) random_int(0, 1)) {
                $product->seo()->save(factory(Seo::class)->make());
            }
            $product->aliases()->saveMany(factory(Alias::class, random_int(2, 6))->make());
        });

        /** @var Product\Option\Set $optionSet */
        $optionSet = (new Product\Option\Set())->newQuery()->where('name', '=', 'MTG Single')->first();
        $permutations = $optionSet->permute();
        $products->random($products->count() / 2)->each(function(Product $product) use ($permutations) {
            $permutations->each(function (array $permutation) use ($product) {
                $permutation = collect($permutation);
                $variation = factory(Product\Variation::class)->make();
                $variation->product()->associate($product);
                $variation->save();
                $variation->options()->sync($permutation->pluck('id')->toArray());
            });
            $product->units_in_stock = 0;
            $product->save();
        });

        $this->seedOrders();
    }

    private function seedOrders()
    {
        $availableProducts = (new Product)->newQuery()
            ->where('disabled', '=', false)
            ->where('units_in_stock', '>', 0)
            ->get();

        $availableVariations = (new Product\Variation)->newQuery()
            ->where('disabled', '=', false)
            ->where('units_in_stock', '>', 0)
            ->get();

        $newStatus = (new Status)->newQuery()->where('name', '=', 'New')->first();

        foreach(range(1, random_int(3, 5)) as $count) {
            $items = collect();
            $availableProducts->random($availableProducts->count() / 5)->each(function (Product $item) use ($items) {
                $items->push([
                    'item' => $item,
                    'quantity' => random_int(1, $item->unitsInStock())
                ]);
            });
            $availableVariations->random($availableVariations->count() / 5)->each(function (Product\Variation $item) use ($items) {
                $items->push([
                    'item' => $item,
                    'quantity' => random_int(1, $item->unitsInStock())
                ]);
            });

            $subtotal = new Money($items->sum(function (array $unit) {
                return $unit['item']->price()->multiply($unit['quantity'])->getAmount();
            }), new Currency(\App\DEFAULT_CURRENCY));

            $tax = \App\calculateTax($subtotal);

            $order = new Order;
            $order->tax = $tax;
            $order->subtotal = $subtotal;
            $order->total = $subtotal->add($tax);
            $order->shipping_cost = new Money(0, new Currency(\App\DEFAULT_CURRENCY));
            $order->paid = false;
            $order->status()->associate($newStatus);
            $order->save();

            $items->each(function (array $item) use ($order) {
                $orderItem = new Order\Item();
                $orderItem->item_id = $item['item']->identifier();
                $orderItem->price = $item['item']->price();
                $orderItem->quantity = $item['quantity'];
                $orderItem->order()->associate($order);
                $orderItem->save();
            });
        }
    }

    private function seedOrderStatuses()
    {
        $names = collect([
            'New',
            'Awaiting Feedback',
            'Awaiting Product Release',
            'Fulfilled',
            'Awaiting Shipment',
            'Shipped',
        ]);

        $names->each(function (string $name) {
            $status = new Status();
            $status->name = $name;
            $status->save();
        });
    }

    private function seedStates()
    {
        $stateData = collect(
            json_decode(
                file_get_contents(__DIR__. implode(DIRECTORY_SEPARATOR, ['', 'data', 'states.json']))
            )
        );

        $stateData->each(function(\stdClass $item) {
            $state = new State();
            $state->name = $item->name;
            $state->saveOrFail();

            $alias = new Alias();
            $alias->content = $item->code;
            $state->aliases()->save($alias);
        });
    }

    private function seedOptions()
    {
        $data = [
            'size' => [
                'small' => [],
                'medium' => [],
                'large' => [],
                'xl' => [
                    'value' => 'Extra Large',
                ],
                'xxl' => [
                    'value' => 'Double XL',
                ],
                'xxxl' => [
                    'value' => 'Triple XL',
                ]
            ],
            'color' => [
                'red' => [],
                'green' => [],
                'blue' => [],
                'indigo' => [],
                'violet' => [],
                'cyan' => [],
                'yellow' => [],
                'pink' => [],
                'purple' => [],
            ],
            'style' => [
                'normal' => [],
                'foil' => [],
            ],
            'condition' => [
                'nm' => [
                    'value' => 'Near Mint/Mint',
                ],
                'sp' => [
                    'value' => 'Slightly Played',
                ],
                'hp' => [
                    'value' => 'Heavily Played'
                ]
            ],
        ];

        foreach ($data as $key => $val) {
            $group = new Product\Option\Group();
            $group->name = ucfirst($key);
            $group->key = $key;
            $group->save();
            foreach ($val as $value => $display) {
                $option = new Product\Option();
                $option->value = $value;
                $option->name = $display['value'] ?? ucfirst($value);
                $option->group()->associate($group);
                $option->save();
            }
        }
    }

    public function seedOptionSets()
    {
        $sets = collect([
            'MTG Single' => [
                'style',
                'condition',
            ],
            'T-Shirt' => [
                'color',
                'size',
            ],
        ]);

        $sets->each(function (array $setKeys, string $name) {
            $set = new Product\Option\Set();
            $set->name = $name;
            $set->save();
            /** @var \Illuminate\Database\Eloquent\Builder $groupQuery */
            $groupQuery = (new Product\Option\Group())
                ->newQuery();
            collect($setKeys)->each(function (string $key) use ($groupQuery) {
                $groupQuery->orWhere('key', '=', $key);
            });
            $groups = $groupQuery->get();
            $groups->each(function(Product\Option\Group $group) use ($set) {
                $group->options->each(function (Product\Option $option) use ($set) {
                    $set->options()->attach($option);
                });
            });
            $set->save();
        });
    }
}
