let button = $('#payment-button');

/* TODO: Look into why this fires twice in device emulation on touch events. */
const handler = StripeCheckout.configure({
  key: button.dataset.stripeKey,
  zipCode: true,
  locale: 'auto',
  token: function (token) {
    $.fetch(button.dataset.action, {
      responseType: 'json',
      method: 'POST',
      data: JSON.stringify({
        'stripe_token': token.id,
        'email': token.email,
      }),
      headers: {
        'Content-type': 'application/json',
        accept: 'application/json',
        'X-CSRF-TOKEN': button.dataset.csrf,
      }
    }).then(response => {
      window.location.href = response.response.redirect;
    });
  }
});

button.addEventListener('click', function () {
  const options = {
    name: button.dataset.name,
    amount: parseInt(button.dataset.amount),
  };

  if ( button.dataset.email ) {
    options.email = button.dataset.email;
  }

  handler.open(options);
});
