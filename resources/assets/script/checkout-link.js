/* global PaymentRequest, $, Stripe */
let checkoutLink = document.querySelector('.app-js-checkout-link');
let checkoutLinkClickEventListener = function(linkClickEvent) {
  if (!window.PaymentRequest) {
    return;
  }
  linkClickEvent.preventDefault();
  let supportedInstruments = [
    {
      supportedMethods: [
        'visa', 'mastercard', 'amex', 'discover'
      ]
    }
  ];
  let details = {
    displayItems: [
      {
        label: 'Products',
        amountAsCents: linkClickEvent.target.dataset.amount,
        amount: {
          currency: 'USD',
          value: linkClickEvent.target.dataset.amount / 100
        }
      }
    ],
    total: {
      label: 'Total',
      amountAsCents: linkClickEvent.target.dataset.amount,
      amount: {
        currency: 'USD',
        value: linkClickEvent.target.dataset.amount / 100
      }
    }
  };
  let options = {
    requestPayerEmail: true,
    requestShipping: true
  };
  let request = new PaymentRequest(supportedInstruments, details, options);
  let shippingUrl = linkClickEvent.target.dataset.routeShipping;
  let paymentUrl = linkClickEvent.target.dataset.routePayment;
  let csrfToken = linkClickEvent.target.dataset.csrf;
  let taxUrl = linkClickEvent.target.dataset.routeTax;
  let isGuest = linkClickEvent.target.dataset.guest === 'true';
  const failDialog = function(message) {
    const dialog = document.createElement('dialog');
    const feedback = document.createElement('p');
    feedback.textContent = message;
    dialog.appendChild(feedback);
    const closeButton = document.createElement('button');
    closeButton.textContent = 'Close';
    closeButton.addEventListener('click', function() {
      dialog.close();
      document.body.removeChild(dialog);
    });
    dialog.appendChild(closeButton);
    document.body.appendChild(dialog);
    dialog.showModal();
  };

  $.fetch(linkClickEvent.target.dataset.routeCsrf, {
    responseType: 'json',
    headers: {
      accept: 'application/json'
    }
  }).then(response => {
    csrfToken = response.response.token;
  });

  request.addEventListener('shippingaddresschange', event => {
    event.updateWith(((details, addr) => {
      /* We do not support shipping outside of the US. */
      if (addr.country !== 'US') {
        details.shippingOptions = [];
        return Promise.resolve(details);
      }

      const options = [];

      if (!isGuest) {
        const pickup = {
          id: 'pickup',
          label: 'In Store Pickup',
          amountInCents: '0',
          amount: {
            currency: 'USD',
            value: '0.00'
          }
        };

        options.push(pickup);
      }
      const addressData = {
        name: addr.recipient,
        streetOne: addr.addressLine[0],
        streetTwo: addr.addressLine[1],
        postcode: addr.postalCode,
        state: addr.region,
        city: addr.city,
        company: addr.organization
      };

      return $.fetch(shippingUrl, {
        responseType: 'json',
        method: 'POST',
        data: JSON.stringify(addressData),
        headers: {
          'Content-type': 'application/json',
          'accept': 'application/json',
          'X-CSRF-TOKEN': csrfToken
        }
      }).then(response => {
        const responseData = response.response;
        options.push({
          id: 'usps',
          label: responseData.provider + ' ' + responseData.service,
          amountAsCents: responseData.price.amount,
          amount: {
            currency: responseData.price.currency,
            value: responseData.price.amount / 100
          }
        });
        details.shippingOptions = options;
        return Promise.resolve(details);
      }).catch(error => {
        details.shippingOptions = options;
        return Promise.resolve(details);
      });
    })(details, request.shippingAddress));
  });

  request.addEventListener('shippingoptionchange', event => {
    event.updateWith(((details, shippingOption) => {
      details.shippingOptions.forEach(option => {
        if (option.id === shippingOption) {
          option.selected = true;
          return;
        }
        option.selected = false;
      });

      const updateDetailsTotal = function() {
        let sum = 0;
        details.displayItems.forEach(item => {
          sum += parseInt(item.amountAsCents, 10);
        });
        details.total.amount.value = sum / 100;
      };
      const selected = details.shippingOptions.filter(option => option.selected)[0];
      const removeItemByLabel = function(label) {
        let existing = details.displayItems.filter(item => item.label === label)[0];

        if (existing) {
          details.displayItems.splice(details.displayItems.indexOf(existing), 1);
        }
      };
      const addTax = function() {
        removeItemByLabel('Tax');
        let taxItem = {
          label: 'Tax',
          amountAsCents: '',
          amount: {
            currency: 'USD',
            value: ''
          }
        };
        return $.fetch(taxUrl, {
          responseType: 'json',
          headers: {
            accept: 'application/json'
          }
        })
          .then(response => {
            const responseData = response.response;
            taxItem.amountAsCents = responseData.amount;
            taxItem.amount.value = responseData.amount / 100;
            details.displayItems.push(taxItem);
            updateDetailsTotal();
            return details;
          });
      };
      if (selected.id === 'pickup') {
        removeItemByLabel('Shipping');
        return addTax().then(details => Promise.resolve(details));
      }

      let address = request.shippingAddress;

      if (address.region.toLowerCase() === 'virginia' ||
        address.region.toLowerCase() === 'va') {
        return addTax().then(details => {
          details.displayItems.push({
            label: 'Shipping',
            amountAsCents: selected.amountAsCents,
            amount: selected.amount
          });
          updateDetailsTotal();
          return Promise.resolve(details);
        });
      }
      removeItemByLabel('Tax');
      details.displayItems.push({
        label: 'Shipping',
        amountAsCents: selected.amountAsCents,
        amount: selected.amount
      });

      updateDetailsTotal();

      return Promise.resolve(details);
    })(details, request.shippingOption));
  });

  request.show()
    .then(paymentResponse => {
      const handler = function(status, response) {
        if (status === 200) {
          $.fetch(paymentUrl, {
            responseType: 'json',
            method: 'POST',
            data: JSON.stringify({
              stripeToken: response.id,
              email: paymentResponse.payerEmail
            }),
            headers: {
              'Content-type': 'application/json',
              'accept': 'application/json',
              'X-CSRF-TOKEN': csrfToken
            }
          })
            .then(response => {
              paymentResponse.complete('success');
              window.location.href = response.response.redirect;
            });
        }
      };
      $.include(self.Stripe, 'https://js.stripe.com/v2/').then(function() {
        Stripe.setPublishableKey(linkClickEvent.target.dataset.stripeKey);
        if (!Stripe.validateCardNumber(paymentResponse.details.cardNumber)) {
          return paymentResponse.complete('fail').then(() => {
            failDialog('Invalid card number provided. The number provided was: ' + paymentResponse.details.cardNumber);
          });
        }
        if (!Stripe.validateCVC(paymentResponse.details.cardSecurityCode)) {
          return paymentResponse.complete('fail').then(() => {
            failDialog('The CVC code provided appears invalid.');
          });
        }
        if (!Stripe.validateExpiry(paymentResponse.details.expiryMonth, paymentResponse.details.expiryYear)) {
          return paymentResponse.complete('fail').then(() => {
            failDialog('The provided card has expired. Please provide new card information.');
          });
        }
        /* eslint-disable quote-props */
        Stripe.card.createToken({
          'number': paymentResponse.details.cardNumber,
          'cvc': paymentResponse.details.cardSecurityCode,
          'exp_month': paymentResponse.details.expiryMonth,
          'exp_year': paymentResponse.details.expiryYear,
          'address_zip': paymentResponse.details.billingAddress.postalCode
        }, handler);
        /* eslint-enable quote-props */
      });
    })
    .catch(err => {
      console.error('Uh oh, something bad happened', err.message);
    });
};

checkoutLink.addEventListener('click', checkoutLinkClickEventListener);
