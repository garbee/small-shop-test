/* global mdl */

let drawer = new mdl.drawer.MDLTemporaryDrawer(document.querySelector('.mdl-temporary-drawer'));
let drawerToggle = document.querySelector('.app-js-temp-drawer-trigger');

drawerToggle.addEventListener('click', function(event) {
  drawer.open = true;
});

document.querySelectorAll('.app-site-menu').forEach(function(menu) {
  menu.querySelectorAll('a').forEach(function(anchor) {
    if (anchor.href === window.location.origin + window.location.pathname) {
      let className = 'mdl-permanent-drawer--selected';
      if (anchor.closest('.mdl-temporary-drawer')) {
        className = 'mdl-temporary-drawer--selected';
      }
      anchor.classList.add(className);
    }
  });
});
