<?php

namespace App\Tests\Features;

use App\Tests\BaseCase;
use App\Mail\Contact as ContactMail;
use Illuminate\Support\Facades\Mail;

class ContactTest extends BaseCase
{
    public function testFormSubmission()
    {
        Mail::fake();
        $this->visitRoute('contact.index')
            ->type('A Name', 'name')
            ->type('test@example.com', 'email')
            ->type('Test Message', 'message')
            ->press('Send Message')
            ->followRedirects()
            ->seeRouteIs('contact.index')
            ->see('Your email has been sent. We\'ll get back to you as soon as possible. Thank you.');
        Mail::assertSent(ContactMail::class, function(ContactMail $mail) {
            return $mail->getInput()->getContent() === 'Test Message';
        });
        Mail::assertSentTo([config('app.contact_email')], ContactMail::class);
    }
}
