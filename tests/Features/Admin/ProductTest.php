<?php

namespace App\Tests\Features\Admin;

use App\Description;
use App\Product;
use App\Tests\BaseCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ProductTest extends BaseCase
{
    use DatabaseMigrations;

    public function testCanBeCreated()
    {
        $payload = [
            'name' => 'Test Product',
            'price' => 10000,
            'units_in_stock' => 12,
            'disabled' => false,
        ];

        $this->post(route('admin.shop.products.store'), $payload);
        $this->seeInDatabase('products', $payload);
    }

    public function testShippingCanBeCreatedWithProduct()
    {
        $payload = [
            'name' => 'Test Product',
            'price' => 10000,
            'units_in_stock' => 12,
            'disabled' => false,
        ];

        $shipping = [
            'shipping' => [
                'weight' => 2,
                'length' => 4,
                'height' => 7,
                'width' => 4,
            ]
        ];

        $this->post(route('admin.shop.products.store'), array_merge($payload, $shipping));
        /** @var Product $product */
        $product = Product::all()->first();

        self::assertEquals(2, $product->shipping->weight->toUnit('pounds'));

        $this->seeInDatabase('shipping', $shipping['shipping']);
    }

    public function testSeoCanBeCreatedWithProduct()
    {
        $payload = [
            'name' => 'Test Product',
            'price' => 10000,
            'units_in_stock' => 12,
            'disabled' => false,
        ];

        $seo = [
            'seo' => [
                'title' => 'Test Title',
                'description' => 'Description of the item for a search engine.',
            ],
        ];

        $this->post(route('admin.shop.products.store'), array_merge($payload, $seo));
        /** @var Product $product */
        $product = Product::all()->first();

        collect($seo['seo'])->each(function(string $value, string $key) use ($product) {
            self::assertEquals($value, $product->seo->{$key});
        });
    }

    public function testDescriptionsCanBeCreatedWithProduct()
    {
        $payload = [
            'name' => 'Test Product',
            'price' => 10000,
            'units_in_stock' => 12,
            'disabled' => false,
        ];

        $descriptions = [
            'descriptions' => [
                'long' => 'Hello long description',
                'short' => 'Short description content.',
            ],
        ];

        $this->post(route('admin.shop.products.store'), array_merge($payload, $descriptions));
        /** @var Product $product */
        $product = Product::all()->first();

        $product->descriptions->each(function (Description $description) use ($descriptions) {
            self::assertEquals($description->content, $descriptions['descriptions'][$description->type]);
        });
    }
}
