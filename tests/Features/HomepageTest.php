<?php

namespace App\Tests\Features;

use App\Tests\BaseCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class HomepageTest extends BaseCase
{
    use DatabaseMigrations;

    public function testHomePageRenders()
    {
        $this->visit('/');
    }
}
