<?php

namespace App\Tests\Features;

use App\Product;
use App\Tests\BaseCase;
use App\Support\GarbeeCart;
use Illuminate\Support\Collection;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CartTest extends BaseCase
{
    use DatabaseMigrations;

    public function testItemCanBeAdded()
    {
        /** @var Product $product */
        $product = factory(Product::class, 1)->create(['units_in_stock' => 10, 'disabled' => false]);

        $this->visitRoute('shop.product.show', ['product' => $product->id]);
        $this->type(2, 'products['. $product->id . '][quantity]');
        $this->press('Add to cart');

        /** @var GarbeeCart $cart */
        $cart = app(GarbeeCart::class);
        self::assertEquals(1, $cart->items()->count());
        self::assertEquals(2, $cart->totalQuantity());
    }

    public function testCartCanBeViewed()
    {
        /** @var Collection $products */
        $products = factory(Product::class, 10)->create(['units_in_stock' => 10, 'disabled' => false]);

        $payload = [
            'products' => []
        ];

        $forCart = $products->random(3);
        $forCart->each(function(Product $item) use (&$payload) {
            $payload['products'][(string) $item->id]['quantity'] = random_int(1, 8);
        });

        $this->post(route('shop.cart.add'), $payload);

        $this->visitRoute('shop.cart.index');

        $forCart->each(function (Product $item) {
            $this->see($item->name);
        });
    }

    public function testCartCanBeCleared()
    {
        /** @var Collection $products */
        $products = factory(Product::class, 10)->create(['units_in_stock' => 10, 'disabled' => false]);

        $payload = [
            'products' => []
        ];

        $forCart = $products->random(3);
        $forCart->each(function(Product $item) use (&$payload) {
            $payload['products'][(string) $item->id]['quantity'] = random_int(1, 8);
        });

        $this->post(route('shop.cart.add'), $payload);

        $this->visitRoute('shop.cart.index')
            ->press('Empty Cart')
            ->followRedirects()
            ->seeRouteIs('shop.cart.index')
            ->see('Your cart is empty.');
    }
}
