import gulp from 'gulp';
import path from 'path';
import sass from 'gulp-sass';
import cssnano from 'gulp-cssnano';
import sourcemaps from 'gulp-sourcemaps';
import webpack from 'webpack-stream';
import rev from 'gulp-rev';
import through from 'through2';

const assetPath = path.resolve(__dirname, 'public', 'a');
const nodeModulesPath = path.resolve(__dirname, 'node_modules');
const manifest = () => rev.manifest('public/a/manifest.json', {
  cwd: 'public/a',
  merge: true
});

gulp.task('material:style', () =>
    gulp.src(path.resolve(__dirname, 'resources', 'assets', 'manual', 'material.scss'))
    .pipe(sourcemaps.init())
    .pipe(sass({
      includePaths: [
        nodeModulesPath
      ]
    }))
    .pipe(cssnano())
    .pipe(rev())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(assetPath))
    .pipe(manifest())
    .pipe(gulp.dest(assetPath))
  );

gulp.task('material:script', () => gulp.src(path.resolve(__dirname, 'resources', 'assets', 'manual', 'material.js'))
    .pipe(webpack({
      output: {
        filename: 'material.js',
        libraryTarget: 'umd',
        library: ['mdc']
      },
      devtool: 'source-map',
      module: {
        loaders: [{
          loader: 'babel-loader'
        }]
      }
    }))
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(through.obj(function(file, enc, cb) {
      // Dont pipe through any source map files as it will be handled
      // by gulp-sourcemaps
      const isSourceMap = /\.map$/.test(file.path);
      if (!isSourceMap) {
        this.push(file);
      }
      cb();
    }))
    .pipe(rev())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(assetPath))
    .pipe(manifest())
    .pipe(gulp.dest(assetPath))
);

gulp.task('app:style', () =>
  gulp.src('resources/assets/style/**/*.css')
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(cssnano())
    .pipe(rev())
    .pipe(gulp.dest(assetPath))
    .pipe(manifest())
    .pipe(gulp.dest(assetPath))
);

gulp.task('default', gulp.series(
  'material:style',
  'material:script'
));
