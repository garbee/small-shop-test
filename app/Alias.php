<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Class Alias
 * @package App
 *
 * @property string $id
 * @property string $content
 * @property Carbon $updated_at
 * @property Carbon $created_at
 */
class Alias extends UuidModel
{
    protected $table = 'aliases';

    protected $fillable = [
        'content',
    ];
}
