<?php

namespace App;

use Carbon\Carbon;
use PhpUnitsOfMeasure\PhysicalQuantity\Length;
use PhpUnitsOfMeasure\PhysicalQuantity\Mass;
use PhpUnitsOfMeasure\PhysicalQuantity\Volume;

/**
 * Class Shippable
 * @package App
 *
 * @property string $id
 * @property Length $width
 * @property Length $height
 * @property Length $length
 * @property Mass $weight
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Shipping extends UuidModel
{
    protected $table = 'shipping';

    protected $fillable = [
        'weight',
        'width',
        'height',
        'length',
    ];

    public function getWeightAttribute(float $value) : Mass
    {
        return new Mass($value, 'pounds');
    }

    public function getWidthAttribute(float $value) : Length
    {
        return new Length($value, 'inches');
    }

    public function getLengthAttribute(float $value) : Length
    {
        return new Length($value, 'inches');
    }

    public function getHeightAttribute(float $value) : Length
    {
        return new Length($value, 'inches');
    }

    public function setWeightAttribute(Mass $value)
    {
        $this->attributes['weight'] = $value->toUnit('pounds');
    }

    public function setWidthAttribute(Length $value)
    {
        $this->attributes['width'] = $value->toUnit('inches');
    }

    public function setLengthAttribute(Length $value)
    {
        $this->attributes['length'] = $value->toUnit('inches');
    }

    public function setHeightAttribute(Length $value)
    {
        $this->attributes['height'] = $value->toUnit('inches');
    }

    public function volume() : Volume
    {
        return new Volume((float) ($this->width * $this->length * $this->height), 'cubic inches');
    }
}
