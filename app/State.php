<?php

namespace App;

use App\Contracts\Aliasable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Kalnoy\Nestedset\NodeTrait;

/**
 * Class State
 * @package App
 *
 * @property string $id
 * @property string $name
 * @property Carbon $updated_at
 * @property Carbon $created_at
 * @property Collection $aliases
 */
class State extends UuidModel implements Aliasable
{
    use NodeTrait;

    protected $table = 'states';

    public function aliases() : BelongsToMany
    {
        return $this->belongsToMany(
            Alias::class,
            'state_aliases',
            'state_id',
            'alias_id'
        );
    }

    public function getLftName() : string
    {
        return 'left';
    }

    public function getRgtName() : string
    {
        return 'right';
    }

    public function getParentIdName() : string
    {
        return 'parent_id';
    }
}
