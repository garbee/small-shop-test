<?php

namespace App\Composers\Partials;

use App\Composers\Composer;
use Illuminate\Auth\AuthManager;
use Illuminate\Contracts\View\View;

class SiteMenu implements Composer
{
    /** @var AuthManager $authManager */
    private $authManager;

    /**
     * SiteMenu constructor.
     *
     * @param \Illuminate\Auth\AuthManager $authManager
     */
    public function __construct(AuthManager $authManager)
    {
        $this->authManager = $authManager;
    }

    public function compose(View $view) : void
    {
        $view->with('user', $this->authManager->guard()->user());
    }

    public static function appliesTo() : string
    {
        return 'partials.siteMenu';
    }
}
