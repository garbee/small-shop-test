<?php

namespace App\Composers\Elements;

use Money\Money;
use Money\Currency;
use App\Composers\Composer;
use App\Support\GarbeeCart;
use Illuminate\Contracts\View\View;
use App\Contracts\Product;
use Garbee\Cart\Contracts\Item;

class CheckoutLink implements Composer
{
    /** @var GarbeeCart $cart */
    private $cart;

    /**
     * CheckoutLink constructor.
     * @param GarbeeCart $cart
     */
    public function __construct(GarbeeCart $cart)
    {
        $this->cart = $cart;
    }


    public function compose(View $view) : void
    {
        $fromDb = $this->cart->itemsFromDatabase();
        $subtotal = new Money($this->cart->items()->sum(function (Item $item) use ($fromDb) {
            $fromDbItem = $fromDb->filter(function(Product $product) use ($item) {
                return $product->identifier() === $item->identifier();
            })->first();
            return $fromDbItem->price->multiply($item->quantity())->getAmount();
        }), new Currency(\App\DEFAULT_CURRENCY));

        $view->with('itemTotal', $subtotal);
    }

    public static function appliesTo() : string
    {
        return 'elements.checkout-link';
    }
}
