<?php

namespace App\Composers\Pages\User;

use App\Composers\Composer;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class Orders implements Composer
{
    /** @var Request $request */
    private $request;

    /**
     * Orders constructor.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function compose(View $view) : void
    {
        $orders = $this->request->user()->orders()->orderBy('created_at', 'desc')->paginate();

        $view->with('orders', $orders);
    }

    public static function appliesTo() : string
    {
        return 'pages.user.orders';
    }
}
