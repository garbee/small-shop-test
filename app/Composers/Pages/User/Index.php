<?php

namespace App\Composers\Pages\User;

use App\Composers\Composer;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;

class Index implements Composer
{
    /** @var Request $request */
    private $request;

    /**
     * Index constructor.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function compose(View $view) : void
    {
        $view->with('user', $this->request->user());
    }

    public static function appliesTo() : string
    {
        return 'pages.user.index';
    }
}
