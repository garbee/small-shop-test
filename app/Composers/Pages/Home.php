<?php

namespace App\Composers\Pages;

use App\Product;
use App\Composers\Composer;
use Illuminate\Contracts\View\View;

class Home implements Composer
{
    /** @var Product $products */
    private $products;

    /**
     * Home constructor.
     *
     * @param Product $products
     */
    public function __construct(Product $products)
    {
        $this->products = $products;
    }

    public function compose(View $view) : void
    {
        $view->with('products', $this->products->latest());
    }

    public static function appliesTo() : string
    {
        return 'pages.home';
    }
}
