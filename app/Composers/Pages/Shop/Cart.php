<?php

namespace App\Composers\Pages\Shop;

use Garbee\Cart\Contracts\Item;
use Money\Money;
use Money\Currency;
use App\Contracts\Product;
use App\Composers\Composer;
use App\Support\GarbeeCart;
use Illuminate\Session\Store;
use Illuminate\Contracts\View\View;

class Cart implements Composer
{
    /** @var GarbeeCart $cart */
    private $cart;

    /** @var Store $session */
    private $session;

    /**
     * Cart constructor.
     * @param GarbeeCart $cart
     * @param Store $session
     */
    public function __construct(GarbeeCart $cart, Store $session)
    {
        $this->cart = $cart;
        $this->session = $session;
    }

    public function compose(View $view) : void
    {
        $items = $this->cart->itemsFromDatabase();
        $shippingEstimate = $this->session->get('shipping.estimate', null);
        $view
            ->with('shippingEstimate', $shippingEstimate)
            ->with('cart', $this->cart)
            ->with('items', $items)
            ->with('subtotal', new Money($this->cart->items()->sum(function (Item $item) use ($items) {
                return $items->reject(function(Product $product) use ($item) {
                    return $product->identifier() !== $item->identifier();
                })->first()->price()->multiply($item->quantity())->getAmount();
            }), new Currency(\App\DEFAULT_CURRENCY)));
    }

    public static function appliesTo() : string
    {
        return 'pages.shop.cart';
    }
}
