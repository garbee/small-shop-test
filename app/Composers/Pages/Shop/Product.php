<?php

namespace App\Composers\Pages\Shop;

use App\Product as Model;
use App\Composers\Composer;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;

class Product implements Composer
{
    /** @var Model $products */
    private $products;

    /** @var Request $request */
    private $request;

    /**
     * Product constructor.
     *
     * @param \App\Product $products
     * @param Request $request
     */
    public function __construct(Model $products, Request $request)
    {
        $this->products = $products;
        $this->request = $request;
    }

    public function compose(View $view) : void
    {
        $view->with(
            'product',
            $this
                ->products
                ->newQuery()
                ->findOrFail($this->request->route('product'))
        );
    }

    public static function appliesTo() : string
    {
        return 'pages.shop.product';
    }
}
