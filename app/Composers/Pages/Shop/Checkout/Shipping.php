<?php

namespace App\Composers\Pages\Shop\Checkout;

use App\Composers\Composer;
use App\State;
use Illuminate\Contracts\View\View;

class Shipping implements Composer
{
    /** @var State $states */
    private $states;

    /**
     * Shipping constructor.
     * @param State $states
     */
    public function __construct(State $states)
    {
        $this->states = $states;
    }

    public function compose(View $view) : void
    {
        $view->with('states', $this->states->all(['id', 'name']));
    }

    public static function appliesTo() : string
    {
        return 'pages.shop.checkout.shipping';
    }
}
