<?php

namespace App\Composers\Pages\Shop\Checkout;

use App\Composers\Composer;
use Garbee\Cart\Item;
use App\Support\GarbeeCart;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Session\Store;
use Money\Money;
use App\Contracts\Product;
use Money\Currency;

class Review implements Composer
{
    /** @var Store $session */
    private $session;

    /** @var \Illuminate\Contracts\Config\Repository */
    private $config;

    /** @var GarbeeCart $cart */
    private $cart;

    /** @var Request $request */
    private $request;

    /**
     * Review constructor.
     * @param Store $session
     * @param \Illuminate\Contracts\Config\Repository $config
     * @param GarbeeCart $cart
     * @param Request $request
     */
    public function __construct(
        Store $session,
        \Illuminate\Contracts\Config\Repository $config,
        GarbeeCart $cart,
        Request $request
    )
    {
        $this->session = $session;
        $this->config = $config;
        $this->cart = $cart;
        $this->request = $request;
    }


    public function compose(View $view) : void
    {
        $estimate = $this->session->get('checkout.shipping.estimate', null);
        $fromDb = $this->cart->itemsFromDatabase();
        $subtotal = new Money($this->cart->items()->sum(function (Item $item) use ($fromDb) {
            $fromDbItem = $fromDb->filter(function(Product $product) use ($item) {
                return $product->identifier() === $item->identifier();
            })->first();
            return $fromDbItem->price->multiply($item->quantity())->getAmount();
        }), new Currency(\App\DEFAULT_CURRENCY));
        $itemSubtotal = $subtotal;
        if ($estimate) {
            $subtotal = $subtotal->add($estimate['price']);
        }

        $view
            ->with('guest', $this->session->get('checkout.guest'))
            ->with('payingOnline', $this->session->get('checkout.paying_online'))
            ->with('itemSubtotal', $itemSubtotal)
            ->with('subtotal', $subtotal)
            ->with('cart', $this->cart)
            ->with('itemsFromDb', $fromDb)
            ->with('shippingDetails', $this->session->get('checkout.shipping.data', null))
            ->with('shippingEstimate', $estimate);
    }

    public static function appliesTo() : string
    {
        return 'pages.shop.checkout.review';
    }
}
