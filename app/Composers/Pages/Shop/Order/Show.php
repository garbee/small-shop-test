<?php

namespace App\Composers\Pages\Shop\Order;

use App\Composers\Composer;
use App\Order;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class Show implements Composer
{
    /** @var Order $orders */
    private $orders;

    /** @var Request $request */
    private $request;

    /**
     * Show constructor.
     * @param Order $orders
     * @param Request $request
     */
    public function __construct(Order $orders, Request $request)
    {
        $this->orders = $orders;
        $this->request = $request;
    }

    public function compose(View $view) : void
    {
        $view->with('order', $this->orders->newQuery()->findOrFail($this->request->route('order')));
    }

    public static function appliesTo() : string
    {
        return 'pages.shop.order.show';
    }
}
