<?php

namespace App\Composers\Layouts;

use App\Composers\Composer;
use Illuminate\Contracts\View\View;
use Illuminate\Contracts\Config\Repository;

class Master implements Composer
{
    /** @var Repository $config */
    private $config;

    /**
     * Home constructor.
     *
     * @param Repository $config
     */
    public function __construct(Repository $config)
    {
        $this->config = $config;
    }

    public function compose(View $view) : void
    {
        $view->with('appName', $this->config->get('app.name'));
    }

    public static function appliesTo() : string
    {
        return 'layouts.master';
    }
}
