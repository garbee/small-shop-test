<?php

namespace App\Composers;

use Illuminate\Contracts\View\View;

interface Composer
{
    public function compose(View $view) : void;

    /**
     * Get the view name the composer should be used for.
     *
     * @return string
     */
    public static function appliesTo() : string;
}
