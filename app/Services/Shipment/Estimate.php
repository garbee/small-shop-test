<?php

namespace App\Services\Shipment;

use Shippo_Address;
use Shippo_Shipment;
use App\Input\Shipping;

class Estimate
{
    public function __invoke(string $postcode, Shipping $shipment) : Shippo_Address
    {
        $from = array_merge([
            'object_purpose' => 'QUOTE',
        ], \App\FROM_ADDRESS);

        $to = [
            'object_purpose' => 'QUOTE',
            'country'        => 'US',
            'zip'            => $postcode,
        ];

        $distanceUnit = 'in';
        $massUnit = 'lb';
        $parcel = [
            'length'        => $shipment->length()->toUnit($distanceUnit),
            'width'         => $shipment->width()->toUnit($distanceUnit),
            'height'        => $shipment->height()->toUnit($distanceUnit),
            'distance_unit' => $distanceUnit,
            'weight'        => $shipment->weight()->toUnit($massUnit),
            'mass_unit'     => $massUnit,
        ];

        $shippoShipment = Shippo_Shipment::create([
            'object_purpose' => 'QUOTE',
            'address_from'   => $from,
            'address_to'     => $to,
            'parcel'         => $parcel,
            'async'          => false,
        ]);

        $rates = collect($shippoShipment->rates_list);

        return $rates->reject(function (Shippo_Address $item) {
            return $item->provider !== 'USPS';
        })
            ->filter(function (Shippo_Address $address) {
                return $address->servicelevel_token === 'usps_priority';
            })
            ->first();
    }
}
