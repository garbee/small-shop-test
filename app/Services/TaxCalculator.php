<?php

namespace App\Services;

use Money\Money;

class TaxCalculator
{
    public function handle(Money $amount) : Money
    {
        return $amount->divide(100)->multiply(\App\TAX_PERCENTAGE);
    }
}
