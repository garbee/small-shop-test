<?php

namespace App\Services;

use App\Mail\Contact as Mail;
use App\Input\Contact as Input;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Mail\Mailer;

class Contact
{
    /** @var Mailer $mailer */
    private $mailer;

    /** @var Repository */
    private $config;

    /**
     * Contact constructor.
     *
     * @param \Illuminate\Contracts\Mail\Mailer $mailer
     * @param \Illuminate\Contracts\Config\Repository $config
     */
    public function __construct(Mailer $mailer, Repository $config)
    {
        $this->mailer = $mailer;
        $this->config = $config;
    }

    public function __invoke(Input $input)
    {
        $this->mailer
            ->to($this->config->get('app.contact_email'))
            ->send(new Mail($input));
    }
}
