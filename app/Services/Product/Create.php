<?php

namespace App\Services\Product;

use App\Product;
use App\Input\Product as Input;

class Create
{
    /** @var Product $products */
    private $products;

    /**
     * Create constructor.
     *
     * @param Product $products
     */
    public function __construct(Product $products)
    {
        $this->products = $products;
    }

    public function __invoke(Input $input) : Product
    {
        $product = $this->products->newInstance();

        $product
            ->fillFromInput($input)
            ->saveOrFail();

        return $product;
    }
}
