<?php

namespace App\Services\Product;

use Exception;
use App\Contracts\Product;
use App\Product as ProductModel;
use App\Product\Variation as VariationModel;

class GetByIdentifier
{
    /** @var ProductModel $products */
    private $products;

    /** @var VariationModel $variations */
    private $variations;

    /**
     * GetByIdentifier constructor.
     * @param ProductModel $products
     * @param VariationModel $variations
     */
    public function __construct(ProductModel $products, VariationModel $variations)
    {
        $this->products = $products;
        $this->variations = $variations;
    }

    public function __invoke(string $identifier) : Product
    {
        /** @var ProductModel|null $product */
        $product = $this->products->newQuery()->find($identifier);

        if ($product) {
            return $product;
        }

        /** @var VariationModel|null $variation */
        $variation = $this->variations->newQuery()->find($identifier);

        if ($variation) {
            return $variation;
        }

        throw new Exception('Could not find a product with the given identifier.');
    }
}
