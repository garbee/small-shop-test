<?php

namespace App\Services\Order;

use App\Order;
use App\User;
use Money\Money;
use Money\Currency;
use App\Contracts\Product;
use App\Support\GarbeeCart;
use Illuminate\Session\Store;
use Garbee\Cart\Contracts\Item as CartItem;
use Illuminate\Database\Connection as DatabaseConnection;

class Create
{
    /**
     * @var Store $session
     */
    private $session;

    /** @var GarbeeCart $cart */
    private $cart;

    /** @var Order $orders */
    private $orders;

    /** @var Order\Item $items */
    private $items;

    /** @var DatabaseConnection $databaseConnection */
    private $databaseConnection;

    /** @var Order\Status $statuses */
    private $statuses;

    /**
     * Create constructor.
     * @param Store $session
     * @param GarbeeCart $cart
     * @param Order $orders
     * @param Order\Item $items
     * @param DatabaseConnection $databaseConnection
     * @param Order\Status $statuses
     */
    public function __construct(
        Store $session,
        GarbeeCart $cart,
        Order $orders,
        Order\Item $items,
        DatabaseConnection $databaseConnection,
        Order\Status $statuses
    )
    {
        $this->session = $session;
        $this->cart = $cart;
        $this->orders = $orders;
        $this->items = $items;
        $this->databaseConnection = $databaseConnection;
        $this->statuses = $statuses;
    }

    public function __invoke(string $email = null, User $user = null) : Order
    {
        $order = $this->orders->newInstance();
        $fromDb = $this->cart->itemsFromDatabase();

        $items = collect();

        $this->cart->items()->each(function(CartItem $item) use ($items, $fromDb) {
            $cartItem = $this->items->newInstance();
            $cartItem->item_id = $item->identifier();
            $cartItem->quantity = $item->quantity();
            $cartItem->price = $fromDb->reject(function(Product $product) use ($item) {
                return $item->identifier() !== $product->identifier();
            })->first()->price();

            $items->push($cartItem);
        });

        $subtotal = new Money($items->sum(function(Order\Item $item) {
            return $item->price->multiply($item->quantity)->getAmount();
        }), new Currency(\App\DEFAULT_CURRENCY));

        if ($this->session->get('checkout.pickup', false)) {
            $order->shipping_cost = new Money(0, new Currency(\App\DEFAULT_CURRENCY));
        } else {
            $shippingCost = $this->session->get('checkout.shipping.estimate.price', null);
            if ($shippingCost === null) {
                throw new \Exception();
            }
            $order->shipping_cost = $shippingCost;
        }

        $order->email = $email;
        $order->subtotal = $subtotal;
        $order->tax = \App\calculateTax($subtotal->add($order->shipping_cost));
        $order->total = $subtotal->add($order->shipping_cost)->add($order->tax);
        $order->paid = false;
        if ($user) {
            $order->user()->associate($user);
        }
        $order->status()->associate($this->statuses->newQuery()->where('name', '=', 'New')->firstOrFail());

        $this->databaseConnection->beginTransaction();
        $order->saveOrFail();
        $order->items()->saveMany($items);
        $this->databaseConnection->commit();

        return $order;
    }
}
