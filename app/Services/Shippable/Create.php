<?php

namespace App\Services\Shippable;

use App\Contracts\Shippable;
use App\Input\Shipping as Input;

class Create
{
    /**
     * @param Input $input
     * @param Shippable $shippable
     *
     * @throws \Exception
     *
     * @return void
     */
    public function __invoke(Input $input, Shippable $shippable)
    {
        $created = $shippable->shipping()->create([
            'width'  => $input->width(),
            'weight' => $input->weight(),
            'height' => $input->height(),
            'length' => $input->length(),
        ]);

        if (! $created) {
            throw new \Exception('Failed to create shippable from input.');
        }
    }
}
