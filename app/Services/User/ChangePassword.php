<?php

namespace App\Services\User;

use App\User;
use App\Input\User\ChangePassword as Input;

class ChangePassword
{
    /**
     * Update the requested users password. Return bool based on success.
     *
     * @param \App\User $user
     * @param \App\Input\User\ChangePassword $input
     *
     * @return bool
     */
    public function __invoke(User $user, Input $input) : bool
    {
        if (! password_verify($input->getCurrentPassword(), $user->password)) {
            return false;
        }

        $user->password = $input->getNewPassword();
        $user->saveOrFail();

        return true;
    }
}
