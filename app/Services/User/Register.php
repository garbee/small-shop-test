<?php

namespace App\Services\User;

use App\Input\User\Registration;
use App\User;

class Register
{
    /** @var User $users */
    private $users;

    /**
     * Register constructor.
     *
     * @param \App\User $users
     */
    public function __construct(User $users)
    {
        $this->users = $users;
    }

    public function __invoke(Registration $input) : User
    {
        $user = $this->users->newInstance();
        $user->name = $input->getName();
        $user->email = $input->getEmail();
        $user->password = $input->getPassword();
        $user->saveOrFail();

        return $user;
    }
}
