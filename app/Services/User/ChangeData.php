<?php

namespace App\Services\User;

use App\User;
use App\Input\User\ChangeData as Input;

class ChangeData
{
    public function __invoke(User $user, Input $input) : User
    {
        $user->name = $input->getName();
        $user->email = $input->getEmail();
        $user->saveOrFail();

        return $user;
    }
}
