<?php

namespace App\Services\Cart;

use App\Contracts\Cart;

class Destroy
{
    /** @var Cart $cart */
    private $cart;

    /**
     * AddItem constructor.
     * @param Cart $cart
     */
    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    public function __invoke() : void
    {
        $this->cart->destroy();
    }
}
