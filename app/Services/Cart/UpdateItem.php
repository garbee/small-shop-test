<?php

namespace App\Services\Cart;

use App\Cart\Item;
use App\Contracts\Cart;
use App\Input\Cart\UpdateItem as Input;

class UpdateItem
{
    /** @var Cart $cart */
    private $cart;

    /**
     * UpdateItem constructor.
     * @param Cart $cart
     */
    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    public function __invoke(Input $input) : void
    {
        if ($input->remove()) {
            $this->cart->remove($input->identifier());

            return;
        }

        /** @var Cart\Item $currentItem */
        $currentItem = $this->cart->items()->filter(function (\Garbee\Cart\Contracts\Item $item) use ($input) {
            return $item->identifier() === $input->identifier();
        })->first();

        $replace = new Item(
            $input->identifier(),
            $currentItem->name(),
            $currentItem->price(),
            $input->quantity()
        );

        $this->cart->remove($input->identifier());
        $this->cart->add($replace);
    }
}
