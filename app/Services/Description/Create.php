<?php

namespace App\Services\Description;

use App\Contracts\Describable;
use App\Input\Description as Input;

class Create
{
    public function __invoke(Input $input, Describable $describable) : void
    {
        if ($input->long()) {
            $describable->descriptions()->create([
                'type'    => 'long',
                'content' => $input->long(),
            ]);
        }

        if ($input->short()) {
            $describable->descriptions()->create([
                'type'    => 'short',
                'content' => $input->short(),
            ]);
        }
    }
}
