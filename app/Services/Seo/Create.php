<?php

namespace App\Services\Seo;

use Exception;
use App\Input\Seo as Input;
use App\Contracts\Seoable;

class Create
{
    public function __invoke(Input $input, Seoable $seoable) : void
    {
        $created = $seoable->seo()->create([
            'title'       => $input->title(),
            'description' => $input->description(),
        ]);

        if (! $created) {
            throw new Exception('Failed to create SEO data.');
        }
    }
}
