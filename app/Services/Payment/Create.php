<?php

namespace App\Services\Payment;

use App\Order;
use App\Payment;
use Stripe\Charge;

class Create
{
    /** @var Payment $payments */
    private $payments;

    /**
     * Create constructor.
     * @param Payment $payments
     */
    public function __construct(Payment $payments)
    {
        $this->payments = $payments;
    }

    public function __invoke(Order $order, string $token, string $email = null) : Payment
    {
        $charge = Charge::create([
            'amount'   => $order->total->getAmount(),
            'currency' => $order->total->getCurrency()->getCode(),
            'source'   => $token,
            'description' => "Order: {$order->id}",
            'capture' => false,
            'metadata' => [
                'forGuest' => is_string($email),
                'guestEmail' => $email,
                'order_id' => $order->id,
            ]
        ]);

        /** @var Payment $payment */
        $payment = $this->payments->newInstance();
        $payment->email = $email;
        $payment->token = $token;
        $payment->amount = $order->total;
        $payment->attempted = false;
        $payment->charged = $charge->captured;
        $payment->authorized = $charge->paid;
        $payment->charge_id = $charge->id;
        $payment->order()->associate($order);
        $payment->save();

        return $payment;
    }
}
