<?php

namespace App;

use App\Product\Variation;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Money\Money;
use Money\Currency;
use App\Input\Product as Input;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Contracts\Product as ProductContract;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * @property string $id
 * @property string $name
 * @property Money $price
 * @property int $units_in_stock
 * @property boolean $disabled
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property Shipping|null $shipping
 * @property Collection|null $descriptions
 * @property Collection|null $variations
 * @property Seo|null $seo
 */
class Product extends UuidModel implements ProductContract
{
    use SoftDeletes;

    protected $table = 'products';

    public $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function setPriceAttribute(Money $value)
    {
        $this->attributes['price'] = $value->getAmount();
    }

    public function getPriceAttribute($value) : Money
    {
        return new Money(
            $value,
            new Currency(\App\DEFAULT_CURRENCY)
        );
    }

    public function aliases() : BelongsToMany
    {
        return $this->belongsToMany(
            Alias::class,
            'product_aliases',
            'product_id',
            'alias_id'
        );
    }

    public function descriptions() : BelongsToMany
    {
        return $this->belongsToMany(
            Description::class,
            'product_descriptions',
            'product_id',
            'description_id'
        );
    }

    public function shipping() : HasOne
    {
        return $this->hasOne(
            Shipping::class,
            'product_id',
            'id'
        );
    }

    public function seo() : HasOne
    {
        return $this->hasOne(
            Seo::class,
            'product_id',
            'id'
        );
    }

    public function variations() : HasMany
    {
        return $this->hasMany(
            Variation::class,
            'product_id',
            'id'
        );
    }

    public function identifier() : string
    {
        return $this->id;
    }

    public function name() : string
    {
        return $this->name;
    }

    public function price() : Money
    {
        return $this->price;
    }

    public function unitsInStock() : int
    {
        return $this->units_in_stock;
    }

    public function hasVariations() : bool
    {
        return ! $this->variations->isEmpty();
    }

    public function isVariation() : bool
    {
        return false;
    }

    public function totalUnitsInStock() : int
    {
        return $this->activeVariations()->sum('units_in_stock');
    }

    public function activeVariations() : Collection
    {
        return $this->variations->where('disabled', '=', false);
    }

    public function latest(int $max = 8) : Collection
    {
        return $this
            ->newQuery()
            ->orderBy('updated_at', 'desc')
            ->take($max)
            ->get();
    }

    public function fillFromInput(Input $input) : self
    {
        $this->name = $input->name();
        $this->units_in_stock = $input->unitsInStock();
        $this->price = $input->price();
        $this->disabled = $input->disabled();

        return $this;
    }
}
