<?php

namespace App;

use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use App\Contracts\Addressable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class User
 * @package App
 *
 * @property string $email
 * @property string $name
 * @property string $id
 * @property string $password
 * @property string $remember_token
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Collection $addresses
 * @property Collection $orders
 */
class User extends Authenticatable implements Addressable
{
    use Notifiable;

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        if (! $this->exists && ! $this->id) {
            $this->id = (string) Uuid::uuid4();
        }
    }

    public function addresses() : BelongsToMany
    {
        return $this->belongsToMany(
            Address::class,
            'user_addresses',
            'user_id',
            'address_id'
        );
    }

    public function orders() : HasMany
    {
        return $this->hasMany(
            Order::class,
            'user_id',
            'id'
        );
    }

    public function setPasswordAttribute(string $value)
    {
        if (password_get_info($value)['algoName'] !== 'bcrypt') {
            $value = password_hash($value, PASSWORD_BCRYPT);
        }

        $this->attributes['password'] = $value;
    }
}
