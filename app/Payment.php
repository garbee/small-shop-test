<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Money\Money;

/**
 * Class Payment
 * @package App
 *
 * @property string $order_id
 * @property string $token
 * @property bool $charged
 * @property bool $attempted
 * @property bool $authorized
 * @property string $charge_id
 * @property Money $amount
 * @property string|null $email
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Order $order
 */
class Payment extends UuidModel
{
    protected $table = 'payments';

    public function setAmountAttribute(Money $value)
    {
        $this->setAttributeFromMoney('amount', $value);
    }

    public function getAmountAttribute($value) : Money
    {
        return $this->getAttributeAsMoney($value);
    }

    public function order() : BelongsTo
    {
        return $this->belongsTo(
            Order::class,
            'order_id',
            'id'
        );
    }
}
