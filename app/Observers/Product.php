<?php

namespace App\Observers;

use Ramsey\Uuid\Uuid;
use App\Product as Model;

class Product implements Observer
{
    public static function appliesTo() : string
    {
        return Model::class;
    }

    public function creating(Model $product)
    {
        if (! isset($product->id)) {
            $product->id = Uuid::uuid4();
        }
    }
}
