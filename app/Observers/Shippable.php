<?php

namespace App\Observers;

use Ramsey\Uuid\Uuid;
use App\Shipping as Model;

class Shippable implements Observer
{
    public static function appliesTo() : string
    {
        return Model::class;
    }

    public function creating(Model $shippable)
    {
        if (! isset($shippable->id)) {
            $shippable->id = Uuid::uuid4();
        }
    }
}
