<?php

namespace App\Observers;

use Ramsey\Uuid\Uuid;
use App\Seo as Model;

class Seo implements Observer
{
    public static function appliesTo() : string
    {
        return Model::class;
    }

    public function creating(Model $seo)
    {
        if (! isset($seo->id)) {
            $seo->id = Uuid::uuid4();
        }
    }
}
