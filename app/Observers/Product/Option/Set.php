<?php

namespace App\Observers\Product\Option;

use Ramsey\Uuid\Uuid;
use App\Observers\Observer;
use App\Product\Option\Set as Model;

class Set implements Observer
{
    public static function appliesTo() : string
    {
        return Model::class;
    }

    public function creating(Model $option)
    {
        if (! isset($option->id)) {
            $option->id = Uuid::uuid4();
        }
    }
}
