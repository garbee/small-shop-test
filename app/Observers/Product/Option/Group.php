<?php

namespace App\Observers\Product\Option;

use Ramsey\Uuid\Uuid;
use App\Observers\Observer;
use App\Product\Option\Group as Model;

class Group implements Observer
{
    public static function appliesTo() : string
    {
        return Model::class;
    }

    public function creating(Model $model)
    {
        if (! isset($model->id)) {
            $model->id = Uuid::uuid4();
        }
    }
}
