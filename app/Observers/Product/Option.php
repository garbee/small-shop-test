<?php

namespace App\Observers\Product;

use Ramsey\Uuid\Uuid;
use App\Observers\Observer;
use App\Product\Option as Model;

class Option implements Observer
{
    public static function appliesTo() : string
    {
        return Model::class;
    }

    public function creating(Model $option)
    {
        if (! isset($option->id)) {
            $option->id = Uuid::uuid4();
        }
    }
}
