<?php

namespace App\Observers\Product;

use Ramsey\Uuid\Uuid;
use App\Observers\Observer;
use App\Product\Variation as Model;

class Variation implements Observer
{
    public static function appliesTo() : string
    {
        return Model::class;
    }

    public function creating(Model $model)
    {
        if (! isset($model->id)) {
            $model->id = Uuid::uuid4();
        }
    }
}
