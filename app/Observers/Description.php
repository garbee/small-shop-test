<?php

namespace App\Observers;

use Ramsey\Uuid\Uuid;
use App\Description as Model;

class Description implements Observer
{
    public static function appliesTo() : string
    {
        return Model::class;
    }

    public function creating(Model $description)
    {
        if (! isset($description->id)) {
            $description->id = Uuid::uuid4();
        }
    }
}
