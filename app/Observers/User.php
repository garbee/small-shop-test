<?php

namespace App\Observers;

use Ramsey\Uuid\Uuid;
use App\User as Model;

class User implements Observer
{
    public static function appliesTo() : string
    {
        return Model::class;
    }

    public function creating(Model $user)
    {
        if (! isset($user->id)) {
            $user->id = Uuid::uuid4();
        }
    }
}
