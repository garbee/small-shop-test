<?php

namespace App\Observers;

use Ramsey\Uuid\Uuid;
use App\Address as Model;

class Address implements Observer
{
    public static function appliesTo() : string
    {
        return Model::class;
    }

    public function creating(Model $model)
    {
        if (! isset($model->id)) {
            $model->id = Uuid::uuid4();
        }
    }
}
