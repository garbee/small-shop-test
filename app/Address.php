<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Address
 * @package App
 *
 * @property string $id
 * @property string $street_one
 * @property string|null $street_two
 * @property string $city
 * @property string $postcode
 * @property string|null $company
 * @property string|null $phone
 * @property string $state_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property State $state
 */
class Address extends UuidModel
{
    protected $table = 'addresses';

    protected $fillable = [
        'street_one',
        'street_two',
        'city',
        'postcode',
        'company',
        'phone',
        'state_id',
    ];

    public function state() : BelongsTo
    {
        return $this->belongsTo(
            State::class,
            'state_id',
            'id'
        );
    }
}
