<?php

namespace App\Support;

use App\Services\Product\GetByIdentifier;
use Money\Money;
use App\Contracts\Cart;
use App\Contracts\Cart\Item;
use Illuminate\Support\Collection;
use Garbee\Cart\Item as GarbeeItem;
use Garbee\Cart\Contracts\Cart as GarbeeCartContract;

class GarbeeCart implements Cart
{
    /** @var GarbeeCartContract $cart */
    private $cart;

    /** @var GetByIdentifier $getProduct */
    private $getProduct;

    /**
     * GarbeeCart constructor.
     * @param GarbeeCartContract $cart
     * @param GetByIdentifier $getProduct
     */
    public function __construct(GarbeeCartContract $cart, GetByIdentifier $getProduct)
    {
        $this->cart = $cart;
        $this->getProduct = $getProduct;
    }

    public function add(Item $item) : Cart
    {
        $this->cart->items()->add(new GarbeeItem(
            $item->id(),
            $item->name(),
            $item->quantity(),
            $item->price()
        ));

        return $this;
    }

    public function remove($cartItemId) : Cart
    {
        $this->cart->items()->remove($cartItemId);

        return $this;
    }

    public function items() : Collection
    {
        return $this->cart->items();
    }

    public function itemsFromDatabase() : Collection
    {
        $items = collect();
        $this->items()->each(function (GarbeeItem $item) use ($items) {
            $items->push($this->getProduct->__invoke($item->identifier()));
        });

        return $items;
    }

    public function subtotal() : Money
    {
        return $this->cart->items()->subtotal();
    }

    public function totalQuantity() : int
    {
        return $this->cart->items()->quantity();
    }

    public function destroy()
    {
        $this->cart->destroy();
    }
}
