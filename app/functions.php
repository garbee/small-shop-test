<?php

namespace App;

use Money\Money;

function calculateTax(Money $amount) : Money
{
    /** @var Services\TaxCalculator $service */
    $service = app(Services\TaxCalculator::class);

    return $service->handle($amount);
}
