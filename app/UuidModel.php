<?php

namespace App;

use Money\Currency;
use Money\Money;
use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UuidModel
 * @package App
 *
 * @property string $id
 */
abstract class UuidModel extends Model
{

    public $incrementing = false;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        if (! $this->exists && ! $this->id) {
            $this->id = (string) Uuid::uuid4();
        }
    }

    protected function setAttributeFromMoney(string $attribute, Money $value)
    {
        $this->attributes[$attribute] = $value->getAmount();
    }

    protected function getAttributeAsMoney($value) : Money
    {
        return new Money($value, new Currency(\App\DEFAULT_CURRENCY));
    }
}
