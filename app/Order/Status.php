<?php

namespace App\Order;

use App\Order;
use App\UuidModel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Status
 * @package App\Order
 *
 * @property string $name
 * @property Collection $orders
 */
class Status extends UuidModel
{
    public $timestamps = false;

    protected $table = 'order_statuses';

    public function orders() : HasMany
    {
        return $this->hasMany(
            Order::class,
            'status_id',
            'id'
        );
    }
}
