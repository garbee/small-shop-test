<?php

namespace App\Order;

use App\Order;
use Money\Money;
use App\UuidModel;
use Carbon\Carbon;
use App\Product\Variation;
use App\Contracts\Product;
use App\Product as ProductModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Item
 * @package App\Order
 *
 * @property string $item_id
 * @property int $quantity
 * @property Money $price
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Order $order
 */
class Item extends UuidModel
{
    protected $table = 'order_items';

    protected $cachedProductLookup;

    public function product() : Product
    {
        if (isset($this->cachedProductLookup)) {
            return $this->cachedProductLookup;
        }

        $product = (new ProductModel)->newQuery()->find($this->item_id);

        if (! $product) {
            $product = (new Variation)->newQuery()->find($this->item_id);
        }

        if (! $product) {
            throw new \Exception;
        }

        $this->cachedProductLookup = $product;

        return $product;
    }

    public function order() : BelongsTo
    {
        return $this->belongsTo(
            Order::class,
            'order_id',
            'id'
        );
    }

    public function getPriceAttribute($value) : Money
    {
        return $this->getAttributeAsMoney($value);
    }

    public function setPriceAttribute(Money $value)
    {
        $this->setAttributeFromMoney('price', $value);
    }
}
