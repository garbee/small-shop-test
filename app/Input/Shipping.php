<?php

namespace App\Input;

use PhpUnitsOfMeasure\PhysicalQuantity\Length;
use PhpUnitsOfMeasure\PhysicalQuantity\Mass;

class Shipping
{
    /** @var Mass $weight */
    private $weight;

    /** @var Length $height */
    private $height;

    /** @var Length $length */
    private $length;

    /** @var Length $width */
    private $width;

    /**
     * Shipping constructor.
     * @param Mass $weight
     * @param Length $height
     * @param Length $length
     * @param Length $width
     */
    public function __construct(
        Mass $weight,
        Length $height,
        Length $length,
        Length $width
    )
    {
        $this->weight = $weight;
        $this->height = $height;
        $this->length = $length;
        $this->width = $width;
    }

    public function weight() : Mass
    {
        return $this->weight;
    }

    public function height() : Length
    {
        return $this->height;
    }

    public function length() : Length
    {
        return $this->length;
    }

    public function width() : Length
    {
        return $this->width;
    }
}
