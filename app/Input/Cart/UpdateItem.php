<?php

namespace App\Input\Cart;

class UpdateItem
{
    /** @var string $identifier */
    private $identifier;

    /** @var int $quantity */
    private $quantity;

    /** @var bool $remove */
    private $remove;

    /**
     * UpdateItem constructor.
     * @param string $identifier
     * @param int $quantity
     * @param bool $remove
     */
    public function __construct(
        string $identifier,
        int $quantity,
        bool $remove
    )
    {
        $this->identifier = $identifier;
        $this->quantity = $quantity;
        $this->remove = $remove;
    }

    public function identifier() : string
    {
        return $this->identifier;
    }

    public function quantity() : int
    {
        return $this->quantity;
    }

    public function remove() : bool
    {
        return $this->remove;
    }
}
