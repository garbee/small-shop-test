<?php

namespace App\Input\User;

class ChangePassword
{
    /** @var string $currentPassword */
    private $currentPassword;

    /** @var string $newPassword */
    private $newPassword;

    /**
     * ChangePassword constructor.
     *
     * @param string $currentPassword
     * @param string $newPassword
     */
    public function __construct(string $currentPassword, string $newPassword)
    {
        $this->currentPassword = $currentPassword;
        $this->newPassword = $newPassword;
    }

    public function getCurrentPassword() : string
    {
        return $this->currentPassword;
    }

    public function getNewPassword() : string
    {
        return $this->newPassword;
    }
}
