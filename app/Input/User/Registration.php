<?php

namespace App\Input\User;

class Registration
{
    /** @var string $name */
    private $name;

    /** @var string $email */
    private $email;

    /** @var string $password */
    private $password;

    /**
     * Registration constructor.
     *
     * @param string $name
     * @param string $email
     * @param string $password
     */
    public function __construct(string $name, string $email, string $password)
    {
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getEmail() : string
    {
        return $this->email;
    }

    public function getPassword() : string
    {
        return $this->password;
    }
}
