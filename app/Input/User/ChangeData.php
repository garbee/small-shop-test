<?php

namespace App\Input\User;

class ChangeData
{
    /** @var string $name */
    private $name;

    /** @var string $email */
    private $email;

    /**
     * ChangeData constructor.
     *
     * @param string $name
     * @param string $email
     */
    public function __construct(string $name, string $email)
    {
        $this->name = $name;
        $this->email = $email;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getEmail() : string
    {
        return $this->email;
    }
}
