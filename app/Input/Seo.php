<?php

namespace App\Input;

class Seo
{
    /** @var string|null $title */
    private $title;

    /** @var string|null $description */
    private $description;

    /**
     * Seo constructor.
     *
     * @param null|string $title
     * @param null|string $description
     */
    public function __construct(? string $title,? string $description)
    {
        $this->title = $title;
        $this->description = $description;
    }

    public function title() : ? string
    {
        return $this->title;
    }

    public function description() : ? string
    {
        return $this->description;
    }
}
