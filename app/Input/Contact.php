<?php

namespace App\Input;

class Contact
{
    /** @var string $name */
    private $name;

    /** @var string $email */
    private $email;

    /** @var string $content */
    private $content;

    /**
     * Contact constructor.
     *
     * @param string $name
     * @param string $email
     * @param string $content
     */
    public function __construct(
        string $name,
        string $email,
        string $content
    )
    {
        $this->name = $name;
        $this->email = $email;
        $this->content = $content;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getEmail() : string
    {
        return $this->email;
    }

    public function getContent() : string
    {
        return $this->content;
    }
}
