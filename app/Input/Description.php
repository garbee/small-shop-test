<?php

namespace App\Input;

class Description
{
    /** @var string|null $long */
    private $long;

    /** @var string|null $short */
    private $short;

    /**
     * Description constructor.
     *
     * @param string|null $long
     * @param string|null $short
     */
    public function __construct(
        ?string $long,
        ?string $short
    )
    {
        $this->long = $long;
        $this->short = $short;
    }

    public function long() : ? string
    {
        return $this->long;
    }

    public function short() : ? string
    {
        return $this->short;
    }
}
