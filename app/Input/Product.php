<?php

namespace App\Input;

use Money\Money;

class Product
{
    /** @var string $name */
    private $name;

    /** @var Money $price */
    private $price;

    /** @var int $unitsInStock */
    private $unitsInStock;

    /** @var bool $disabled */
    private $disabled;

    /**
     * Product constructor.
     *
     * @param string $name
     * @param Money $price
     * @param int $unitsInStock
     * @param bool $disabled
     */
    public function __construct(
        string $name,
        Money $price,
        int $unitsInStock,
        bool $disabled = false
    )
    {
        $this->name = $name;
        $this->price = $price;
        $this->unitsInStock = $unitsInStock;
        $this->disabled = $disabled;
    }

    public function name() : string
    {
        return $this->name;
    }

    public function price() : Money
    {
        return $this->price;
    }

    public function unitsInStock() : int
    {
        return $this->unitsInStock;
    }

    public function disabled() : bool
    {
        return $this->disabled;
    }
}
