<?php

namespace App\Providers\Routes;

use App\Http\Controllers\Shop\Checkout;
use App\Http\Controllers\Shop\Checkout\Shipping;
use App\Http\Controllers\Shop\Order;
use App\Http\Controllers\Shop\Tax;
use App\Http\Requests\Shop\Checkout\Index;
use Illuminate\Routing\Router;
use App\Http\Controllers\Shop\Cart;
use App\Http\Controllers\Api\Shop\Products as ProductsApiController;
use App\Http\Controllers\Shop\Product as ProductController;
use App\Http\Controllers\Shop\Shipping\Estimate as ShippingEstimateController;

class Shop extends Base
{
    public static function registerApiRoutes(Router $router)
    {
        $router->group(['prefix' => 'shop'], function () use ($router) {
            $router->get('products', self::uses(ProductsApiController::class, 'index'));
        });
    }

    public static function registerWebRoutes(Router $router)
    {
        $router->group(['prefix' => 'shop', 'as' => 'shop.'], function () use ($router) {
            $router->resource('product', ProductController::class, [
                'only' => [
                    'index',
                    'show',
                ],
            ]);

            $router->get('tax', [
                'uses' => self::uses(Tax::class, 'index'),
                'as' => 'tax'
            ]);

            $router->group(['prefix' => 'cart'], function () use ($router) {
                $router->get('/', [
                    'uses' => self::uses(Cart::class, 'index'),
                    'as'   => 'cart.index',
                ]);
                $router->post('add', [
                    'uses' => self::uses(Cart::class, 'add'),
                    'as'   => 'cart.add',
                ]);
                $router->post('update', [
                    'uses' => self::uses(Cart::class, 'store'),
                    'as'   => 'cart.update',
                ]);
                $router->delete('/', [
                    'uses' => self::uses(Cart::class, 'destroy'),
                    'as'   => 'cart.destroy',
                ]);
            });

            $router->group(['prefix' => 'shipping'], function () use ($router) {
                $router->get('estimate', [
                    'uses' => self::uses(ShippingEstimateController::class, 'retrieve'),
                    'as'   => 'shipping.estimate',
                ]);
            });

            $router->group(['prefix' => 'checkout'], function () use ($router) {
                $router->get('/', [
                    'uses' => self::uses(Checkout::class, 'index'),
                    'as' => 'checkout.index',
                ]);
                $router->post('/', [
                    'uses' => self::uses(Checkout::class, 'submit'),
                    'as' => 'checkout.submit',
                ]);
                $router->get('shipping', [
                    'uses' => self::uses(Shipping::class, 'index'),
                    'as'   => 'checkout.shipping',
                ]);
                $router->post('shipping', [
                    'uses' => self::uses(Shipping::class, 'submit'),
                    'as'   => 'checkout.shipping.submit'
                ]);
                $router->get('review', [
                    'uses' => self::uses(Checkout\Review::class, 'index'),
                    'as'   => 'checkout.review',
                ]);
                $router->post('review', [
                    'uses' => self::uses(Checkout\Review::class, 'submit'),
                    'as'   => 'checkout.review.submit'
                ]);
            });

            $router->group(['prefix' => 'order'], function () use ($router) {
                $router->get('{order}/request', [
                    'uses' => self::uses(Order::class, 'index'),
                    'as'   => 'order.show.request'
                ]);
                $router->match(['GET', 'POST'], '{order}', [
                    'uses' => self::uses(Order::class, 'show'),
                    'as' => 'order.show',
                ]);
            });

        });
    }
}
