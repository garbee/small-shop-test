<?php

namespace App\Providers\Routes;

use Illuminate\Routing\Router;
use \App\Http\Controllers\User as Controller;

class User extends Base
{
    public static function registerApiRoutes(Router $router)
    {
        // TODO: Implement registerApiRoutes() method.
    }

    public static function registerWebRoutes(Router $router)
    {
        $router->group(['prefix' => 'account', 'middleware' => 'auth'], function () use ($router) {
            $router->get('/', [
                'uses' => self::uses(Controller::class, 'index'),
                'as' => 'account'
            ]);
            $router->post('/', [
                'uses' => self::uses(Controller::class, 'store'),
                'as' => 'account.edit.submit',
            ]);
            $router->post('password', [
                'uses' => self::uses(Controller::class, 'changePassword'),
                'as' => 'account.password.submit',
            ]);
            $router->get('orders', [
                'uses' => self::uses(Controller::class, 'orders'),
                'as' => 'account.orders'
            ]);
        });
    }
}
