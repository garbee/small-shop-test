<?php

namespace App\Providers\Routes;

use Illuminate\Routing\Router;
use App\Http\Controllers\Admin\Product;

class Admin extends Base
{
    public static function registerApiRoutes(Router $router)
    {
        // TODO: Implement registerApiRoutes() method.
    }

    public static function registerWebRoutes(Router $router)
    {
        $router->group(
            [
                'prefix' => 'admin',
                'as'     => 'admin.',
            ],
            function (Router $router) {
                $router->group(
                    [
                        'prefix' => 'shop',
                        'as'     => 'shop.',
                    ],
                    function (Router $router) {
                        $router->resource(
                            'products',
                            Product::class
                        );
                    });
            });
    }
}
