<?php

namespace App\Providers\Routes;

use App\Http\Controllers\Auth;
use App\Http\Controllers\Home;
use App\Http\Controllers\Register;
use Illuminate\Routing\Router;
use App\Http\Controllers\Csrf;
use App\Http\Controllers\Contact;

class StaticPages extends Base
{

    /**
     * @inheritdoc
     */
    public static function registerApiRoutes(Router $router)
    {
    }

    /**
     * @inheritdoc
     */
    public static function registerWebRoutes(Router $router)
    {
        $router->get('/', [
            'as'   => 'home',
            'uses' => self::uses(Home::class, 'index'),
        ]);
        $router->get('csrf', [
            'as' => 'csrf',
            'uses' => self::uses(Csrf::class, 'index'),
        ]);

        $router->get('register', [
            'as' => 'register',
            'uses' => self::uses(Register::class, 'index'),
        ]);
        $router->post('register', [
            'as' => 'register.submit',
            'uses' => self::uses(Register::class, 'store'),
        ]);

        $router->resource('login', Auth::class, [
            'only' => [
                'index',
                'store',
            ],
            'names' => [
                'index' => 'login'
            ]
        ]);
        $router->delete('login', [
            'as' => 'login.destroy',
            'uses' => self::uses(Auth::class, 'destroy'),
        ]);

        $router->resource('contact', Contact::class, [
            'only' => [
                'index',
                'store',
            ],
        ]);
    }
}
