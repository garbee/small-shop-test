<?php

namespace App\Providers;

use App\Contracts\Product;
use App\Input\Shipping;
use PhpUnitsOfMeasure\PhysicalQuantity\Length;
use PhpUnitsOfMeasure\PhysicalQuantity\Mass;
use Shippo;
use Garbee\Cart\Cart;
use App\Support\GarbeeCart;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;
use Garbee\Cart\Support\Illuminate\Session;
use Illuminate\Contracts\Foundation\Application;
use Stripe\Stripe;

class App extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (! $this->app->environment() !== 'production') {
            $this->app->register(\Clockwork\Support\Laravel\ClockworkServiceProvider::class);
        }

        $this->registerCart();
        $this->registerCollectionMacros();

        Shippo::setApiKey($this->app['config']['services.shippo.key']);
        Stripe::setApiKey($this->app['config']['services.stripe.secret']);
    }

    private function registerCart()
    {
        $this->app['garbee.cart.session'] = $this->app->share(function (Application $app) {
            return new Session($app['session.store'], 'cart', 'main');
        });

        $this->app['garbee.cart'] = $this->app->share(function (Application $app) {
            return new Cart($app['garbee.cart.session']);
        });

        $this->app->bind(\App\Contracts\Cart::class, GarbeeCart::class);
        $this->app->bind(\Garbee\Cart\Contracts\Cart::class, 'garbee.cart');
    }

    private function registerCollectionMacros()
    {
        /**
         * Macro from : https://tpavlek.me/blog/2016/07/05/laravel-collection-macro-unique-combinations/
         */
        Collection::macro('combinations', function (Collection $combineWith, array $keys = [0, 1]) {
            /** @var Collection $this */
            return $this->reduce(function (Collection $combinations, $originalItem) use ($combineWith, $keys) {
                return $combinations->push($combineWith->map(function ($otherItem) use ($originalItem, $keys) {
                    return [$keys[0] => $originalItem, $keys[1] => $otherItem];
                }));
            }, new static)
                ->flatten(1);
        });

        Collection::macro('createShippingFromProducts', function () : Shipping {
            /** @var Collection $items */
            /** @var Collection $this */
            $items = $this->map(function (Product $item) {
                if (!$item->shipping) {
                    return $item->product->shipping;
                }
                return $item->shipping;
            });

            $weight = new Mass($items->sum(function (\App\Shipping $shipping) {
                return $shipping->weight->toUnit('ounces');
            }), 'lb');
            $height = new Length($items->sum(function (\App\Shipping $shipping) {
                return $shipping->height->toUnit('in');
            }), 'in');
            $width = new Length($items->sum(function (\App\Shipping $shipping) {
                return $shipping->width->toUnit('in');
            }), 'in');
            $length = new Length($items->sum(function (\App\Shipping $shipping) {
                return $shipping->length->toUnit('in');
            }), 'in');

            return new Shipping($weight, $height, $length, $width);
        });
    }
}
