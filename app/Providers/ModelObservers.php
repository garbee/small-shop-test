<?php

namespace App\Providers;

use App\Observers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;

class ModelObservers extends ServiceProvider
{
    public static $observers = [
        Observers\Alias::class,
        Observers\Address::class,
        Observers\Description::class,
        Observers\Product::class,
        Observers\Seo::class,
        Observers\Shippable::class,
        Observers\User::class,
        Observers\Product\Option::class,
        Observers\Product\Option\Group::class,
        Observers\Product\Option\Set::class,
        Observers\Product\Variation::class,
        Observers\State::class,
    ];

    public function boot()
    {
        collect(self::$observers)
            ->each(function (string $class) {
                /** @var Observers\Observer $class */
                /** @var Model $model */
                $model = $class::appliesTo();
                $model::observe($class);
            });
    }
}
