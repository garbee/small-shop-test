<?php

namespace App\Providers;

use App\Order;
use App\User;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class Auth extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->gateDefinitions($this->app->make(Gate::class));
    }

    private function gateDefinitions(Gate $gate)
    {
        $gate->define('view-order', function (User $user, Order $order) : bool {
            if ($order->user) {
                return $user->id === $order->user->id;
            }
            return $order->email === $user->email;
        });
    }
}
