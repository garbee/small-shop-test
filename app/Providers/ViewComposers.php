<?php
namespace App\Providers;

use App\Composers;
use App\Composers\Composer;
use Illuminate\View\Factory;
use Illuminate\Support\ServiceProvider;

class ViewComposers extends ServiceProvider
{
    public static $composers = [
        Composers\Layouts\Master::class,
        Composers\Pages\Home::class,
        Composers\Pages\Shop\Cart::class,
        Composers\Pages\Shop\Product::class,
        Composers\Pages\Shop\ProductIndex::class,
        Composers\Pages\Shop\Checkout\Shipping::class,
        Composers\Pages\Shop\Checkout\Review::class,
        Composers\Elements\CheckoutLink::class,
        Composers\Pages\Shop\Order\Show::class,
        Composers\Partials\SiteMenu::class,
        Composers\Pages\User\Orders::class,
    ];

    public function register()
    {
        $this->registerComposers($this->app['view']);
    }

    private function registerComposers(Factory $view)
    {
        $view->share('user', $this->app['request']->user());
        $view->share('isGuest', ! (bool) $this->app['request']->user());
        collect(self::$composers)
            ->each(function (string $class) use ($view) {
                /** @var Composer $class */
                $view->composer($class::appliesTo(), $class);
            });
    }
}
