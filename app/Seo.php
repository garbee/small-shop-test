<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Class Seo
 *
 * @package App
 *
 * @property string $id
 * @property string|null $title
 * @property string|null $description
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Seo extends UuidModel
{
    protected $table = 'seo';

    protected $fillable = [
        'title',
        'description',
    ];
}
