<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Product as Model;

class Product extends TransformerAbstract
{
    public function transform(Model $product) : array
    {
        return [
            'id'           => (string) $product->id,
            'name'         => $product->name,
            'price'        => $product->price->jsonSerialize(),
            'unitsInStock' => $product->units_in_stock,
            'disabled'     => (bool) $product->disabled,
            'updatedAt'    => $product->updated_at->toW3cString(),
        ];
    }
}
