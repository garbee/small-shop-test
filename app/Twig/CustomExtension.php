<?php

namespace App\Twig;

use App\Currency;
use Twig_Extension;
use Twig_SimpleFilter;
use Twig_SimpleFunction;

class CustomExtension extends Twig_Extension
{
    /** @var Currency $currency */
    private $currency;

    /**
     * CustomExtension constructor.
     * @param Currency $currency
     */
    public function __construct(Currency $currency)
    {
        $this->currency = $currency;
    }

    public function getName() : string
    {
        return 'Custom_Extension';
    }

    public function getFilters() : array
    {
        return [
            new Twig_SimpleFilter('formatMoney', [$this->currency, 'formatMoney']),
        ];
    }

    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('calculateTax', 'App\calculateTax'),
        ];
    }
}
