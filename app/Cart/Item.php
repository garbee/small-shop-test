<?php

namespace App\Cart;

use App\Contracts\Cart\Item as Contract;
use Money\Currency;
use Money\Money;
use stdClass;

class Item implements Contract
{
    /** @var string $id */
    private $id;

    /** @var string $name */
    private $name;

    /** @var Money $price */
    private $price;

    /** @var int $quantity */
    private $quantity;

    /**
     * Item constructor.
     *
     * @param string $id
     * @param string $name
     * @param Money $price
     * @param int $quantity
     */
    public function __construct(
        string $id,
        string $name,
        Money $price,
        int $quantity
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->quantity = $quantity;
    }


    public function id() : string
    {
        return $this->id;
    }

    public function name() : string
    {
        return $this->name;
    }

    public function price() : Money
    {
        return $this->price;
    }

    public function quantity() : int
    {
        return $this->quantity;
    }

    public static function fromStdObj(stdClass $obj) : Contract
    {
        return new self(
            $obj->id,
            $obj->name,
            new Money(
                $obj->price->amount,
                new Currency($obj->price->currency)
            ),
            $obj->quantity
        );
    }

    public function jsonSerialize() : array
    {
        return [
            'id'       => $this->id,
            'name'     => $this->name,
            'price'    => [
                'amount'   => $this->price->getAmount(),
                'currency' => $this->price->getCurrency(),
            ],
            'quantity' => $this->quantity,
        ];
    }
}
