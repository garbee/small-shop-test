<?php

namespace App;

/**
 * Class Description
 * @package App
 *
 * @property string $id
 * @property string $type
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Description extends UuidModel
{
    protected $table = 'descriptions';

    protected $fillable = [
        'type',
        'content',
    ];
}
