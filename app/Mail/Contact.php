<?php

namespace App\Mail;

use App\Input\Contact as Input;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contact extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /** @var \App\Input\Contact $input */
    private $input;

    /**
     * Create a new message instance.
     *
     * @param \App\Input\Contact $input
     */
    public function __construct(Input $input)
    {
        $this->input = $input;
    }

    public function getInput() : Input
    {
        return $this->input;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('emails.contact')
            ->from($this->input->getEmail(), $this->input->getName())
            ->with('content', $this->input->getContent())
            ->with('fromEmail', $this->input->getEmail())
            ->with('fromName', $this->input->getName());
    }
}
