<?php

namespace App\Http\Middleware;

use Closure;

class StripIndex
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->isMethod('GET') && str_contains($request->fullUrl(), 'index.php')) {
            return redirect(str_replace(['index.php/', 'index.php'], '', $request->fullUrl()));
        }

        return $next($request);
    }
}
