<?php

namespace App\Http\Controllers;

use App\Input\Contact as Input;
use App\Services\Contact as ContactService;
use App\Http\Requests\Contact as StoreRequest;
use Symfony\Component\HttpFoundation\Response;

class Contact extends Controller
{
    public function index() : Response
    {
        return $this->response->view('pages.contact');
    }

    public function store(StoreRequest $request, ContactService $service) : Response
    {
        $service(new Input(
            $request->getNameInput(),
            $request->getEmailInput(),
            $request->getMessageInput()
        ));

        $request->session()->flash(
            'message',
            'Your email has been sent. We\'ll get back to you as soon as possible. Thank you.'
        );

        return $this->response->redirectToRoute('contact.index');
    }
}
