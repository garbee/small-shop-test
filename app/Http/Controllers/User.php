<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\ChangeData;
use App\Http\Requests\User\ChangePassword;
use App\Input\User\ChangeData as ChangeInput;
use Symfony\Component\HttpFoundation\Response;
use App\Services\User\ChangeData as ChangeService;
use App\Input\User\ChangePassword as PasswordInput;
use App\Services\User\ChangePassword as PasswordService;

class User extends Controller
{
    public function index() : Response
    {
        return $this->response->view('pages.user.index');
    }

    public function orders() : Response
    {
        return $this->response->view('pages.user.orders');
    }

    public function store(ChangeData $request, ChangeService $service) : Response
    {
        $service(
            $request->user(),
            new ChangeInput(
                $request->input('name'),
                $request->input('email')
            )
        );

        $request->session()->flash('message', 'Changes saved successfully.');

        return $this->response->redirectToRoute('account');
    }

    public function changePassword(ChangePassword $request, PasswordService $service) : Response
    {
        $success = $service($request->user(), new PasswordInput(
            $request->input('current-password'),
            $request->input('new-password')
        ));

        $request->session()->flash(
            'message',
            $success ? 'Password updated successfully.' : 'Password not updated.'
        );

        return $this->response->redirectToRoute('account');
    }
}
