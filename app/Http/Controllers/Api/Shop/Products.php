<?php

namespace App\Http\Controllers\Api\Shop;

use App\Http\Requests\Api as Request;
use League\Fractal\Resource\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Product;
use App\Transformers\Product as ProductTransformer;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;

class Products extends Controller
{
    public function index(Request $request) : Response
    {
        $this->fractalParseIncludesAndExcludes($request);

        $paginator = (new Product)->newQuery()->paginate($request->input('per_page', 15));

        $paginator->appends($request->except('page'));

        $resource = new Collection($paginator->getCollection(), new ProductTransformer);
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));

        return $this->response->json($this->fractal->createData($resource)->toArray());
    }
}
