<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use League\Fractal\Manager;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /** @var ResponseFactory $response */
    protected $response;

    /** @var Manager $fractal */
    protected $fractal;

    public function __construct(
        ResponseFactory $responseFactory,
        Manager $fractal
    )
    {
        $this->response = $responseFactory;
        $this->fractal = $fractal;
    }

    protected function fractalParseIncludesAndExcludes(Request $request)
    {
        if ($request->has('includes')) {
            $this->fractal->parseIncludes($request->input('includes'));
        }

        if ($request->has('excludes')) {
            $this->fractal->parseExcludes($request->input('excludes'));
        }
    }
}
