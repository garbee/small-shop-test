<?php

namespace App\Http\Controllers\Admin;

use Money\Money;
use Money\Currency;
use App\Input\Seo as SeoInput;
use App\Input\Product as Input;
use App\Http\Controllers\Controller;
use App\Input\Shipping as ShippingInput;
use PhpUnitsOfMeasure\PhysicalQuantity\Length;
use PhpUnitsOfMeasure\PhysicalQuantity\Mass;
use Symfony\Component\HttpFoundation\Response;
use App\Input\Description as DescriptionInput;
use App\Services\Seo\Create as CreateSeoService;
use App\Services\Product\Create as CreateService;
use App\Services\Shippable\Create as CreateShippable;
use App\Http\Requests\Admin\Product as ProductRequest;
use App\Services\Description\Create as CreateDescriptionService;

class Product extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    public function store(
        ProductRequest $request,
        CreateService $service,
        CreateShippable $shippableService,
        CreateSeoService $seoService,
        CreateDescriptionService $descriptionService
    ) : Response
    {
        $product = $service(new Input(
            $request->getNameInput(),
            new Money(
                $request->getPriceInput(),
                new Currency(\App\DEFAULT_CURRENCY)
            ),
            $request->getUnitsInStockInput(),
            $request->getDisabledInput()
        ));

        if ($shipping = $request->getShippingInput()) {
            $shippableService(
                new ShippingInput(
                    new Mass($shipping['weight'], 'pounds'),
                    new Length($shipping['height'], 'inches'),
                    new Length($shipping['length'], 'inches'),
                    new Length($shipping['width'], 'inches')
                ),
                $product
            );
        }

        if ($descriptions = $request->getDescriptionsInput()) {
            $descriptionService(
                new DescriptionInput(
                    $descriptions['long'] ?? null,
                    $descriptions['short'] ?? null
                ),
                $product
            );
        }

        if ($seo = $request->getSeoInput()) {
            $seoService(new SeoInput(
                $seo['title'] ?? null,
                $seo['description'] ?? null
            ), $product);
        }

        return $this->response->json(['message' => 'Build']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
