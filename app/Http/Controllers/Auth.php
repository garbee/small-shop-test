<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\Login;
use App\Http\Requests\User\Logout;
use Illuminate\Auth\AuthManager;
use Symfony\Component\HttpFoundation\Response;

class Auth extends Controller
{
    public function index() : Response
    {
        return $this->response->view('pages.login');
    }

    public function store(Login $request, AuthManager $authManager) : Response
    {
        if ($authManager->guard()->attempt([
            'email' => $request->getEmailInput(),
            'password' => $request->getPasswordInput(),
        ],
            $request->getRememberMeInput()
        )) {
            return $this->response->redirectToIntended();
        }

        $request->session()->flash('messages', ['Invalid credentials']);

        return $this->response->redirectToRoute('login');
    }

    public function destroy(Logout $request, AuthManager $authManager) : Response
    {
        $authManager->guard()->logout();

        return $this->response->redirectToRoute('home');
    }
}
