<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class Csrf extends Controller
{
    public function index(Request $request) : Response
    {
        if (!$request->expectsJson()) {
            return $this->response->redirectToRoute('home');
        }

        return $this->response->json(['token' => csrf_token()]);
    }
}
