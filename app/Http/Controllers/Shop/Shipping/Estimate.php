<?php

namespace App\Http\Controllers\Shop\Shipping;

use Money\Money;
use Money\Currency;
use App\Support\GarbeeCart;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Services\Shipment\Estimate as Service;
use App\Http\Requests\Shop\Shipping\Estimate as EstimateRequest;

class Estimate extends Controller
{
    public function retrieve(
        GarbeeCart $cart,
        EstimateRequest $request,
        Service $service
    ) : Response
    {
        if ($cart->items()->isEmpty()) {
            return $this->response->redirectToRoute('shop.cart.index');
        }

        $itemsFromDb = $cart->itemsFromDatabase();
        $estimate = $service(
            $request->getPostcodeInput(),
            $itemsFromDb->createShippingFromProducts()
        );

        $data = [
            'price'    => new Money(bcmul($estimate->amount, 100), new Currency(\App\DEFAULT_CURRENCY)),
            'provider' => $estimate->provider,
            'service'  => $estimate->servicelevel_name,
            'terms'    => $estimate->duration_terms,
        ];

        if ($request->expectsJson()) {
            return $this->response->json($data);
        }

        $request->session()->flash('shipping.estimate', $data);

        return $this->response->redirectToRoute('shop.cart.index');
    }
}
