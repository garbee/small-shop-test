<?php

namespace App\Http\Controllers\Shop;

use Money\Money;
use Money\Currency;
use App\Contracts\Product;
use App\Support\GarbeeCart;
use Illuminate\Http\Request;
use Garbee\Cart\Contracts\Item;
use App\Services\TaxCalculator;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;

class Tax extends Controller
{
    public function index(
        TaxCalculator $service,
        Request $request,
        GarbeeCart $cart
    ) : Response
    {
        $fromDb = $cart->itemsFromDatabase();
        $subtotal = new Money($cart->items()->sum(function (Item $item) use ($fromDb) {
            $fromDbItem = $fromDb->filter(function(Product $product) use ($item) {
                return $product->identifier() === $item->identifier();
            })->first();
            return $fromDbItem->price->multiply($item->quantity())->getAmount();
        }), new Currency(\App\DEFAULT_CURRENCY));

        if ($request->session()->has('checkout.shipping.estimate.price')) {
            $subtotal = $subtotal->add($request->session()->get('checkout.shipping.estimate.price'));
        }

        $amount = $service->handle($subtotal);

        return $this->response->json($amount);
    }
}
