<?php

namespace App\Http\Controllers\Shop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\Shop\Checkout\Index as IndexRequest;

class Checkout extends Controller
{
    public function index(Request $request) : Response
    {
        $request->session()->forget('checkout');

        if (!$request->user()) {
            $request->session()->set('checkout', [
                'pickup' => false,
                'paying_online' => true,
                'guest' => true,
            ]);
            return $this->response->redirectToRoute('shop.checkout.shipping');
        }

        return $this->response->view('pages.shop.checkout.index');
    }

    public function submit(IndexRequest $request) : Response
    {
        $payingOnline = $request->getPayingOnlineInput();
        $pickup = $request->getPickupInput();

        $sessionData = [];

        $sessionData['pickup'] = $pickup;
        $sessionData['paying_online'] = $pickup === false ? true : $payingOnline;
        $sessionData['guest'] = false;

        $request->session()->set('checkout', $sessionData);

        if (!$pickup) {
            return $this->response->redirectToRoute('shop.checkout.shipping');
        }

        return $this->response->redirectToRoute('shop.checkout.review');
    }
}
