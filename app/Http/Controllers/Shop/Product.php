<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;

class Product extends Controller
{
    public function index() : Response
    {
        return $this->response->view('pages.shop.product.index');
    }

    public function show() : Response
    {
        return $this->response->view('pages.shop.product');
    }
}
