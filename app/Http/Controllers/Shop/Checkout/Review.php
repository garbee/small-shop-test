<?php

namespace App\Http\Controllers\Shop\Checkout;

use App\Http\Controllers\Controller;
use App\Http\Requests\Shop\Checkout\Payment;
use App\Services\Cart\Destroy;
use Symfony\Component\HttpFoundation\Response;
use App\Services\Order\Create as CreateOrderService;
use App\Services\Payment\Create as CreatePaymentService;

class Review extends Controller
{
    public function index() : Response
    {
        return $this->response->view('pages.shop.checkout.review');
    }

    public function submit(
        Payment $paymentRequest,
        CreateOrderService $createOrderService,
        CreatePaymentService $createPaymentService,
        Destroy $destroyCartService
    ) : Response
    {
        $token = $paymentRequest->getStripeTokenInput();
        $email = $paymentRequest->getEmailInput();
        $order = $createOrderService($email, $paymentRequest->user());

        if ($token) {
            $createPaymentService($order, $token, $email);
        }

        $paymentRequest->session()->remove('checkout');
        $destroyCartService();

        $redirect = route('shop.order.show', ['order' => $order->id]);

        if ($paymentRequest->expectsJson()) {
            return $this->response->json([
                'redirect' => $redirect,
            ]);
        }

        return $this->response->redirectTo($redirect);
    }
}
