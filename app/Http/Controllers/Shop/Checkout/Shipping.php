<?php

namespace App\Http\Controllers\Shop\Checkout;

use App\Http\Controllers\Controller;
use App\Http\Requests\Shop\Checkout\Shipping as SubmitRequest;
use App\Services\Shipment\Estimate;
use App\Support\GarbeeCart;
use Money\Currency;
use Money\Money;
use Symfony\Component\HttpFoundation\Response;

class Shipping extends Controller
{
    public function index() : Response
    {
        return $this->response->view('pages.shop.checkout.shipping');
    }

    public function submit(
        SubmitRequest $request,
        Estimate $shippingEstimateService,
        GarbeeCart $cart
    ) : Response
    {
        $input = collect([
            'name',
            'streetOne',
            'streetTwo',
            'postcode',
            'state',
            'city',
            'company',
        ])->flatMap(function (string $key) use ($request) {
            return [$key => $request->input($key, null)];
        });

        $estimate = $shippingEstimateService(
            $input['postcode'],
            $cart->itemsFromDatabase()->createShippingFromProducts()
        );

        $data = [
            'price'    => new Money(bcmul($estimate->amount, 100), new Currency(\App\DEFAULT_CURRENCY)),
            'provider' => $estimate->provider,
            'service'  => $estimate->servicelevel_name,
            'terms'    => $estimate->duration_terms,
        ];

        $request->session()->put('checkout.shipping.estimate', $data);
        $request->session()->put('checkout.shipping.data', $input);

        if ($request->expectsJson()) {
            return $this->response->json($data);
        }

        return $this->response->redirectToRoute('shop.checkout.review');
    }
}
