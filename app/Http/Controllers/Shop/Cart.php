<?php

namespace App\Http\Controllers\Shop;

use App\Cart\Item;
use App\Http\Controllers\Controller;
use App\Input\Cart\UpdateItem;
use App\Services\Cart\AddItem as AddService;
use App\Http\Requests\Cart\Update as UpdateRequest;
use App\Services\Cart\UpdateItem as UpdateItemService;
use Symfony\Component\HttpFoundation\Response;
use App\Services\Cart\Destroy as DestroyService;
use App\Http\Requests\Shop\Cart\AddItem as AddRequest;
use App\Services\Product\GetByIdentifier as GetProduct;

class Cart extends Controller
{
    public function index() : Response
    {
        return $this->response->view('pages.shop.cart');
    }

    public function add(
        AddRequest $request,
        AddService $service,
        GetProduct $getService
    ) : Response
    {
        collect($request->getProductsInput())
            ->map(function ($item) {
                $item['quantity'] = (int) $item['quantity'];

                return $item;
            })
            ->reject(function (array $item) {
                return $item['quantity'] === 0;
            })
            ->each(function (array $item, string $identifier) use ($getService, $service) {
                $product = $getService($identifier);

                $service(new Item(
                    $product->identifier(),
                    $product->name(),
                    $product->price(),
                    $item['quantity']
                ));
            });

        return redirect()->back();
    }

    public function store(
        UpdateRequest $request,
        UpdateItemService $service
    ) : Response
    {
        collect($request->getProductsInput())->each(function (array $item, string $identifier) use ($service) {
            $service(new UpdateItem($identifier, (int) $item['quantity'], (bool) $item['remove']));
        });

        return $this->response->redirectToRoute('shop.cart.index');
    }

    public function destroy(DestroyService $service) : Response
    {
        $service();

        return $this->response->redirectToRoute('shop.cart.index');
    }
}
