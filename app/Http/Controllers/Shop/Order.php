<?php

namespace App\Http\Controllers\Shop;

use App\Http\Requests\Order\Show;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;

class Order extends Controller
{
    public function index() : Response
    {
        return $this->response->view('pages.shop.order.request');
    }

    public function show(Show $showRequest) : Response
    {
        return $this->response->view('pages.shop.order.show');
    }
}
