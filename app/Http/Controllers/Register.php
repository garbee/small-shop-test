<?php

namespace App\Http\Controllers;

use App\Services\User\Register as Service;
use App\Http\Requests\User\Registration;
use Illuminate\Auth\AuthManager;
use Symfony\Component\HttpFoundation\Response;

class Register extends Controller
{
    public function index() : Response
    {
        return $this->response->view('pages.register');
    }

    public function store(Registration $request, Service $service, AuthManager $authManager) : Response
    {
        $input = new \App\Input\User\Registration(
            $request->input('name'),
            $request->input('email'),
            $request->input('password')
        );

        $user = $service($input);

        $authManager->guard()->login($user);

        return $this->response->redirectToRoute('home');
    }
}
