<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class Logout extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return (bool) $this->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        // No data needs to be submitted for this.
        return [];
    }
}
