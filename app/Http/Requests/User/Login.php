<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class Login extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return (bool) ! $this->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'email' => [
                'required',
                'email',
            ],
            'password' => [
                'required',
                'string',
                'min:8',
                'max:100',
            ],
            'rememberMe' => [
                'required',
                'boolean',
            ]
        ];
    }

    public function getEmailInput() : string
    {
        return $this->input('email');
    }

    public function getPasswordInput() : string
    {
        return $this->input('password');
    }

    public function getRememberMeInput() : bool
    {
        return (bool) $this->input('rememberMe');
    }
}
