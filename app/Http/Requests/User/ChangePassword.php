<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class ChangePassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return (bool) $this->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'current-password' => [
                'required',
                'string',
                'min:8',
                'max:100',
            ],
            'new-password' => [
                'required',
                'string',
                'min:8',
                'max:100',
            ],
        ];
    }
}
