<?php

namespace App\Http\Requests\Order;

use App\Order;
use App\User;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Foundation\Http\FormRequest;
use App\Exceptions\Order\Show\EmailRequired;

class Show extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @throws EmailRequired
     *
     * @return bool
     */
    public function authorize() : bool
    {
        /** @var Order $order */
        $order = (new Order)->newQuery()->findOrFail($this->route('order'));
        /** @var Gate $gate */
        $gate = app(Gate::class);

        $user = $this->user() ?? new User();
        if (!$user->exists) {
            $email = $this->input('email', null);

            if ($email === null) {
                throw new EmailRequired();
            }

            $user->email = $email;
        }

        return $gate->forUser($user)->allows('view-order', $order);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [];
    }
}
