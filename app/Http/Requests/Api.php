<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Api extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'per_page' => [
                'sometimes',
                'integer',
                'max:450',
            ],
            'includes' => [
                'sometimes',
                'string',
            ],
            'excludes' => [
                'sometimes',
                'string',
            ],
        ];
    }

    public function messages() : array
    {
        return [
            'per_page.max' => 'You may not request more than 450 items per page.',
        ];
    }
}
