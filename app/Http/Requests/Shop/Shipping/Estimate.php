<?php

namespace App\Http\Requests\Shop\Shipping;

use Illuminate\Foundation\Http\FormRequest;

class Estimate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'postcode' => [
                'required',
                'string',
            ],
        ];
    }

    public function getPostcodeInput() : string
    {
        return $this->input('postcode');
    }
}
