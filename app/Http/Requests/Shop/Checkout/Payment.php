<?php

namespace App\Http\Requests\Shop\Checkout;

use Illuminate\Foundation\Http\FormRequest;

class Payment extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        $rules = [
            'stripeToken' => [
                'sometimes',
                'string',
            ],
            'email' => [
                'sometimes',
                'email',
            ]
        ];

        if ((bool) $this->user() === false) {
            $rules['email'] = ['required', 'email'];
        }

        return $rules;
    }

    public function getStripeTokenInput() : ? string
    {
        return $this->input('stripeToken', null);
    }

    public function getEmailInput() : ? string
    {
        return $this->input('email', null);
    }
}
