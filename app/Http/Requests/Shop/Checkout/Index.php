<?php

namespace App\Http\Requests\Shop\Checkout;

use Illuminate\Foundation\Http\FormRequest;

class Index extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'pickup' => [
                'required',
                'boolean',
            ],
            'paying_online' => [
                'required',
                'boolean',
            ]
        ];
    }

    public function getPickupInput() : bool
    {
        return (bool) $this->input('pickup');
    }

    public function getPayingOnlineInput() : bool
    {
        return (bool) $this->input('paying_online');
    }
}
