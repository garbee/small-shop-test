<?php

namespace App\Http\Requests\Shop\Checkout;

use Illuminate\Foundation\Http\FormRequest;

class Shipping extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'name'       => [
                'required',
                'string',
            ],
            'streetOne' => [
                'required',
                'string',
            ],
            'postcode' => [
                'required',
                'string',
            ],
            'state' => [
                'required',
                'string',
            ],
            'city' => [
                'required',
                'string',
            ],
        ];
    }
}
