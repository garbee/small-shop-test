<?php

namespace App\Http\Requests\Shop\Cart;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AddItem extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'products'            => [
                'required',
                'array',
            ],
            'products.*.quantity' => [
                'sometimes',
                'int',
            ],
        ];
    }

    public function getProductsInput() : array
    {
        return $this->input('products');
    }
}
