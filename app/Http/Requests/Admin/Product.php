<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Product extends FormRequest
{
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'name'           => [
                'required',
            ],
            'price'          => [
                'required',
            ],
            'units_in_stock' => [
                'required',
                'integer',
            ],
            'disabled' => [
                'required',
                'boolean',
            ],
            'shipping.*'     => [
                'sometimes',
                'int',
            ],
            'descriptions.*' => [
                'sometimes',
                'string',
            ],
            'seo.*'          => [
                'sometimes',
                'string',
            ],
        ];
    }

    public function getNameInput() : string
    {
        return $this->input('name');
    }

    public function getPriceInput() : string
    {
        return $this->input('price');
    }

    public function getDisabledInput() : bool
    {
        return (bool) $this->input('disabled');
    }

    public function getUnitsInStockInput() : int
    {
        return $this->input('units_in_stock');
    }

    public function getShippingInput() : ? array
    {
        return $this->input('shipping', null);
    }

    public function getDescriptionsInput() : ? array
    {
        return $this->input('descriptions', null);
    }

    public function getSeoInput() : ? array
    {
        return $this->input('seo', null);
    }
}
