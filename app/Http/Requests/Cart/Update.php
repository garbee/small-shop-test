<?php

namespace App\Http\Requests\Cart;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'products'            => [
                'required',
                'array',
            ],
            'products.*.quantity' => [
                'sometimes',
                'int',
            ],
            'products.*.remove'   => [
                'required',
                'boolean',
            ],
        ];
    }

    public function getProductsInput() : array
    {
        return $this->input('products');
    }
}
