<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Contact extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'name' => [
                'required',
                'string',
            ],
            'email' => [
                'required',
                'email',
            ],
            'message' => [
                'required',
                'string',
            ],
        ];
    }

    public function getNameInput() : string
    {
        return $this->input('name');
    }

    public function getEmailInput() : string
    {
        return $this->input('email');
    }

    public function getMessageInput() : string
    {
        return $this->input('message');
    }
}
