<?php

namespace App\Product;

use App\UuidModel;
use Carbon\Carbon;
use App\Product\Option\Set;
use App\Product\Option\Group;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Option
 * @package App\Product
 *
 * @property string $id
 * @property string $group_id
 * @property string $name
 * @property string $value
 * @property Group $group
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Collection $sets
 */
class Option extends UuidModel
{
    protected $table = 'product_options';

    public function group() : BelongsTo
    {
        return $this->belongsTo(Group::class, 'group_id', 'id');
    }

    public function sets() : BelongsToMany
    {
        return $this->belongsToMany(
            Set::class,
            'product_option_set_options',
            'option_id',
            'option_set_id'
        );
    }

    public function variations() : BelongsToMany
    {
        return $this->belongsToMany(
            Variation::class,
            'product_variation_options',
            'product_option_id',
            'product_variation_id'
        );
    }
}
