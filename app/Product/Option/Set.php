<?php

namespace App\Product\Option;

use App\UuidModel;
use Carbon\Carbon;
use App\Product\Option;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection as BaseCollection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Set
 * @package App\Product\Option
 *
 * @property string $id
 * @property string $name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Collection $options
 */
class Set extends UuidModel
{
    use SoftDeletes;

    protected $table = 'product_option_sets';

    public function options() : BelongsToMany
    {
        return $this->belongsToMany(
            Option::class,
            'product_option_set_options',
            'option_set_id',
            'option_id'
        );
    }

    public function optionGroups() : BaseCollection
    {
        return $this->options->map(function (Option $option) {
            return $option->group;
        })->unique();
    }

    /**
     * Permute all available options for the set.
     *
     * To be clear, this is a complete black box.
     * Pulled the code from somewhere and hacked around it to get it working.
     * No idea what it does really, but it works.
     *
     * @return BaseCollection
     */
    public function permute() : BaseCollection
    {
        $groups = $this->optionGroups();
        $output = $groups->shift()->options;

        $groups->each(function (Group $group) use (&$output) {
            $output = $output->combinations($group->options);
        });

        return $output;
    }
}
