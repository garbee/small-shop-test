<?php

namespace App\Product\Option;

use App\UuidModel;
use Carbon\Carbon;
use App\Product\Option;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Groups
 * @package App\Product\Option
 *
 * @property string $id
 * @property string $name
 * @property string $key
 * @property Collection $options
 * @property Carbon $updated_at
 * @property Carbon $created_at
 */
class Group extends UuidModel
{
    protected $table = 'product_option_groups';

    public function options() : HasMany
    {
        return $this->hasMany(
            Option::class,
            'group_id',
            'id'
        );
    }
}
