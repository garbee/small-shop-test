<?php

namespace App\Product;

use App\UuidModel;
use App\Alias;
use App\Seo;
use App\Product;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Money\Money;
use App\Shipping;
use Carbon\Carbon;
use Money\Currency;
use App\Description;
use App\Contracts\Product as ProductContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Variation
 *
 * @package App\Product
 *
 * @property string $id
 * @property string $product_id
 * @property Money $price
 * @property int $units_in_stock
 * @property bool $disabled
 * @property bool $default
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property Product $product
 * @property Shipping|null $shipping
 */
class Variation extends UuidModel implements ProductContract
{
    use SoftDeletes;

    protected $table = 'product_variations';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function product() : BelongsTo
    {
        return $this->belongsTo(
            Product::class,
            'product_id',
            'id'
        );
    }

    public function options() : BelongsToMany
    {
        return $this->belongsToMany(
            Option::class,
            'product_variation_options',
            'product_variation_id',
            'product_option_id'
        );
    }

    public function aliases() : BelongsToMany
    {
        return $this->belongsToMany(
            Alias::class,
            'product_variation_aliases',
            'variation_id',
            'alias_id'
        );
    }

    public function shipping() : HasOne
    {
        return $this->hasOne(
            Shipping::class,
            'product_variation_id',
            'id'
        );
    }

    /**
     * @param Money|null $value
     */
    public function setPriceAttribute($value)
    {
        /** @var Money $value */
        $this->attributes['price'] = is_null($value) ? null : $value->getAmount();
    }

    /**
     * @param int|null $value
     *
     * @return Money
     */
    public function getPriceAttribute($value) : Money
    {
        if (is_null($value)) {
            return $this->product->price;
        }

        return new Money(
            $value,
            new Currency(\App\DEFAULT_CURRENCY)
        );
    }

    public function descriptions() : BelongsToMany
    {
        return $this->belongsToMany(
            Description::class,
            'product_variation_descriptions',
            'product_id',
            'description_id'
        );
    }

    public function identifier() : string
    {
        return $this->id;
    }

    public function name() : string
    {
        return $this->product->name;
    }

    public function price() : Money
    {
        if ($this->attributes['price']) {
            return $this->price;
        }

        return $this->product->price;
    }

    public function unitsInStock() : int
    {
        return $this->units_in_stock;
    }

    public function isVariation() : bool
    {
        return true;
    }

    public function seo() : HasOne
    {
        return $this->hasOne(
            Seo::class,
            'product_variation_id',
            'id'
        );
    }
}
