<?php

namespace App\Console\Commands\Make;

class Input extends Base
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:input {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new input class';

    protected function getDefaultNamespace($rootNamespace) : string
    {
        return $rootNamespace . '\Input';
    }

    protected function getStub() : string
    {
        return $this->getStubPath('input.php.stub');
    }
}
