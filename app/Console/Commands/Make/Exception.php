<?php

namespace App\Console\Commands\Make;

class Exception extends Base
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:exception {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new exception class';

    protected function getStub() : string
    {
        return $this->getStubPath('exception.php.stub');
    }

    protected function getDefaultNamespace($rootNamespace) : string
    {
        return $rootNamespace . '\Exceptions';
    }
}
