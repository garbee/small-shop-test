<?php

namespace App;

use App\Order\Status;
use Money\Money;
use Carbon\Carbon;
use App\Order\Item;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Order
 * @package App
 *
 * @property Money $subtotal
 * @property Money $shipping_cost
 * @property Money $tax
 * @property Money $total
 * @property bool $paid
 * @property string|null $email
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Collection $payments
 * @property Collection $items
 * @property User|null $user
 * @property Status $status
 */
class Order extends UuidModel
{
    protected $table = 'orders';

    public function status() : BelongsTo
    {
        return $this->belongsTo(
            Status::class,
            'status_id',
            'id'
        );
    }

    public function user() : BelongsTo
    {
        return $this->belongsTo(
            User::class,
            'user_id',
            'id'
        );
    }

    public function items() : HasMany
    {
        return $this->hasMany(
            Item::class,
            'order_id',
            'id'
        );
    }

    public function payments() : HasMany
    {
        return $this->hasMany(
            Payment::class,
            'order_id',
            'id'
        );
    }

    public function setSubtotalAttribute(Money $value)
    {
        $this->setAttributeFromMoney('subtotal', $value);
    }

    public function getSubtotalAttribute($value)
    {
        return $this->getAttributeAsMoney($value);
    }

    public function setShippingCostAttribute(Money $value)
    {
        $this->setAttributeFromMoney('shipping_cost', $value);
    }

    public function getShippingCostAttribute($value)
    {
        return $this->getAttributeAsMoney($value);
    }

    public function setTaxAttribute(Money $value)
    {
        $this->setAttributeFromMoney('tax', $value);
    }

    public function getTaxAttribute($value)
    {
        return $this->getAttributeAsMoney($value);
    }

    public function setTotalAttribute(Money $value)
    {
        $this->setAttributeFromMoney('total', $value);
    }

    public function getTotalAttribute($value)
    {
        return $this->getAttributeAsMoney($value);
    }
}
