<?php

namespace App;

const DEFAULT_CURRENCY = 'USD';

const TAX_PERCENTAGE = '5.3';

const FROM_ADDRESS = [
    'name'    => 'Mr Hippo',
    'company' => 'Shippo',
    'street1' => '215 Clayton St.',
    'city'    => 'San Francisco',
    'state'   => 'CA',
    'zip'     => '94117',
    'country' => 'US',
    'phone'   => '+1 555 341 9393',
    'email'   => 'mr-hippo@goshippo.com',
];
