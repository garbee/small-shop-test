<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Relations\HasOne;

interface Shippable
{
    public function shipping() : HasOne;
}
