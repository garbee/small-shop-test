<?php

namespace App\Contracts\Cart;

use stdClass;
use Money\Money;
use JsonSerializable;

interface Item extends JsonSerializable
{
    public function id() : string;

    public function name() : string;

    public function price() : Money;

    public function quantity() : int;

    public static function fromStdObj(stdClass $obj) : self;
}
