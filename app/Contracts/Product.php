<?php

namespace App\Contracts;

use Money\Money;

interface Product extends
    Aliasable,
    Shippable,
    Describable,
    Seoable
{
    public function identifier() : string;

    public function name() : string;

    public function price() : Money;

    public function unitsInStock() : int;

    public function isVariation() : bool;
}
