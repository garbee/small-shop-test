<?php

namespace App\Contracts;

use App\Contracts\Cart\Item;
use Illuminate\Support\Collection;
use Money\Money;

interface Cart
{
    public function add(Item $item) : Cart;

    public function remove($cartItemId) : Cart;

    public function items() : Collection;

    public function subtotal() : Money;

    public function totalQuantity() : int;

    public function itemsFromDatabase() : Collection;

    /**
     * @return void
     */
    public function destroy();
}
