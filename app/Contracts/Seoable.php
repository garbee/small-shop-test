<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Relations\HasOne;

interface Seoable
{
    public function seo() : HasOne;
}
