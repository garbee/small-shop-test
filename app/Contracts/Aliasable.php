<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

interface Aliasable
{
    public function aliases() : BelongsToMany;
}
