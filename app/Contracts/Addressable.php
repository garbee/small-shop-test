<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

interface Addressable
{
    public function addresses() : BelongsToMany;
}
