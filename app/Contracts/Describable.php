<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

interface Describable
{
    public function descriptions() : BelongsToMany;
}
